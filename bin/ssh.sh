#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} -l Downloads/file.txt -r ~/Documents"
    echo -e "\t -i --Set remote ip adress (default: ${remote_ip_addr})"
    echo -e "\t -l --Set local/host path (default: ${local_path})"
    echo -e "\t -r --Set remote path (default: ${remote_path})"
    echo -e "\t -H --Set hostname (default: ${hostname})"
    echo -e "\t -R --Set remotename (default: ${remotename})"
    exit 2
}

hostname='zadca123'
remotename='zadca123'
remote_ip_addr='192.168.1.19'
remote_path="$HOME/ssh"
local_path="$HOME/ssh"
while getopts ':i:dp:r:R:l:H:h' opt; do
    case "$opt" in
        i) remote_ip_addr=${OPTARG} ;;
        l) local_path=${OPTARG} ;;
        r) remote_path=${OPTARG} ;;
        H) hostname=${OPTARG} ;;
        R) remotename=${OPTARG} ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))
[[ -z $local_path ]] && display_help

ssh "${remotename}@${remote_ip_addr}"

# rsync include exclude pattern examples:
# "*"         means everything
# "dir1"      transfers empty directory [dir1]
# "dir*"      transfers empty directories like: "dir1", "dir2", "dir3", etc...
# "file*"     transfers files whose names start with [file]
# "dir**"     transfers every path that starts with [dir] like "dir1/file.txt", "dir2/bar/ffaa.html", etc...
# "dir***"    same as above
# "dir1/*"    does nothing
# "dir1/**"   does nothing
# "dir1/***"  transfers [dir1] directory and all its contents like "dir1/file.txt", "dir1/fooo.sh", "dir1/fold/baar.py", etc...
