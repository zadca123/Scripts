#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# ref_url="https://github.com/GloriousEggroll/proton-ge-custom/releases/latest"
ref_url="https://github.com/GloriousEggroll/proton-ge-custom/releases"
release_ext=".tar.gz"

a=$(2>/dev/null curl $ref_url | sed -r "s/.+href=\"(.+)\" rel=.+/\1/" | grep -E "^.+releases.+tar.gz$" | sort -n | tail -n1)
dl="https://github.com$a"
echo downloading $dl
wget "$dl" -O "proton_ge_latest${release_ext}"

mkdir -p ~/.steam/root/compatibilitytools.d/
tar -xavf "proton_ge_latest${release_ext}" \
    -C ~/.steam/root/compatibilitytools.d/
