#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# Is there a better way to get easily parseable ISBN information on a book?
# I made the script below, but after a few queries it locks up with a captcha and the output that I get is unusable because of it.

# With this script, after reading a book I can input its ISBN and it will download an HTML document that I can open with firefox that looks like pic related.

# MEDIADIR is exported in my /etc/profile.

# I'm not sure if adding the "-q" to wget would mess up the output.
BOOKPATH="$MEDIADIR/documents/books/read"
read -p "Search for ISBN: " answer
wget https://isbnsearch.org/isbn/$answer.tmp && cat $BOOKPATH/$answer.tmp | head -n 50 > $BOOKPATH/$answer && rm -rf $BOOKPATH/$answer.tmp

# Any help would be appreciated.
