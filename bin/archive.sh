#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} [ext] <file/s>"
    exit 2
}


# if extension is not provided, use tar.gz
if [[ -e "${1:-}" ]]; then
    EXTENSION="tar.gz"
else
    EXTENSION="$1"
    shift 1
fi

[[ $# -lt 1 ]] && display_help

case "$EXTENSION" in
    zip)   cmd="zip -r9" ;;
    7z)    cmd="7z a -t7z -mx=9" ;;
    tar)   cmd="tar -uvf" ;;
    tar.*) cmd="tar -cavf" ;;
    # tar.zst)  cmd="tar -c -I'zstd --ultra -filefile -T0' -vf" ;;
    h|\?|:|*) display_help ;;
esac

DATE="$(date +'%F_%H%M%S')"
$cmd "archive_${DATE}_${1}.${EXTENSION}" -- "$@"
