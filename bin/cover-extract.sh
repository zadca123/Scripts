#!/usr/bin/env bash
set -euo pipefail # bash strict mode
if [ "$#" -eq 1 ];then
    ffmpeg -loglevel 0 -y -i "$1" "$PWD/cover.png"
else
    echo Usage "$0": '[audio-file]'
fi
