#!/usr/bin/env bash
set -euo pipefail # bash strict mode

DATE="$(date +%Y/%m/%d)" # yyyy/mm/dd
DATE_IGNORE_PATTERN="20[0-9][0-9]"
DIR="${HOME}/Downloads" # no slash at the end!
DATE_DIR="${DIR}/${DATE}/"
BIG_FILES="big_files"
BIG_FILES_DIR="${DIR}/${BIG_FILES}/"


move_files(){
    mapfile -t files < <(find "$DIR" -maxdepth 1 -mindepth 1 -not -name "$DATE_IGNORE_PATTERN" -not -name "$BIG_FILES" -not -name "*.part")
    if [[ ${#files[@]} -eq 0 ]]; then
        echo "No files to organize"
        exit 2
    fi
    mkdir -pv "$DATE_DIR"
    mv -v "${files[@]}" "$DATE_DIR"
}
del_empty_files(){
    find "$DIR" -empty -delete
}
move_big_files(){
    mapfile -t files < <(find "$DIR" -type f -size +1G -not -name "$BIG_FILES" -not -name "*.part")
    if [[ ${#files[@]} -eq 0 ]]; then
        echo "No files to organize"
        exit 2
    fi
    mkdir -pv "$BIG_FILES_DIR"
    mv -v "${files[@]}" "$BIG_FILES_DIR"
}


read -rp "Are your sure you want to move everything from ${DIR}/* to ${DATE_DIR} (Y[es]/N): " ans
if [[ 'yes' =~ ^${ans,,} ]]; then
    move_files
    move_big_files
    del_empty_files
fi
