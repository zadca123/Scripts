#!/usr/bin/env bash
set -euo pipefail # bash strict mode
pidof dwmblocks >/dev/null || dwmblocks

exit
