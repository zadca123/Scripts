#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} -iname [pattern]"
    exit 2
}

error() {
    echo "$0: $*" >&2
    exit 1
}

[[ $# -lt 1 ]] && display_help

date=$(date +'%F_%H%M%S')
pattern="$@"
DIR="/tmp/drag-drop_${pattern}_${date}/"
mkdir -p "$DIR"

find "$PWD" -type f -iname "$pattern" -print0 -exec ln -s {} "$DIR" \;
# find $PWD -type f -name "*.docx" -print0 | xargs -0 -I{} -- echo qwe; ln -s {} /tmp/qwerty

nautilus "$DIR"
