#!/usr/bin/env bash
set -euo pipefail # bash strict mode
pdflatex -interaction nonstopmode "$1"
# pdflatex -interaction nonstopmode "$1" "${1%.*}.pdf"
