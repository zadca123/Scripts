#!/usr/bin/env bash
set -euo pipefail # bash strict mode

everything="${@:1:$#-1}"
last="${!#}"
last="${@: -1}"

echo "everything except last element: $everything"
echo "last element: $last"
