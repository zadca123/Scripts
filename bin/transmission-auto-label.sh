#!/usr/bin/env bash
set -euo pipefail # bash strict mode

LABELS=(games ttc music movies art books audiobooks)
ROOT_DIR="/media/${USER}/Seagate8TB/multimedia"

error(){
    echo "YOU HAVE SOMETHING WRONG WITH SCRIPT!"
    echo "Your label is: '${1}', is not defined!"
    exit 2
}

display_help(){
    echo "Usage: ${0##*/} [label|set_download_dir]"
    exit 2
}

label_torrents(){
    for label in "${LABELS[@]}"; do
        strings_by_label "$label"
        for str in "${strings[@]}"; do
            transmission-remote -F n:"$str" -L "$label"
            echo "transmission-remote -F n:${str} -L ${label}"
            transmission-remote -F n:"${str,,}" -L "$label"
            echo "transmission-remote -F n:${str,,} -L ${label}"
            transmission-remote -F n:"${str^^}" -L "$label"
            echo "transmission-remote -F n:${str^^} -L ${label}"
            str=${str,,}; transmission-remote -F n:"${str^}" -L "$label"
            echo "transmission-remote -F n:${str^} -L ${label}"
        done
    done
}

strings_by_label(){
    label="$1"
    case "${label,,}" in
        movies) strings=(mkv mp4 avi 1080p 720p 480p YTS.AM YTS.AM Tigole BluRay x265  5.1 7.1 film);;
        music) strings=(flac ogg opus album discography дискография);;
        books) strings=(book eBook kindle epub azw pdf mobi cbr cbz DjVu eReader nicolubbin calibre) ;;
        audiobooks) strings=(AudioBook ZlotoPolsky SuperProdukcja mp3 abridged) ;;
        games) strings=(GoG PlayStation XboX Nintendo GameCube) ;;
        art) strings=(art ArtBook painting WallPaper) ;;
        ttc) strings=(ttc TheTeachingCompany tgc TheGreatCourses) ;;
        *) error "$label";;
    esac
}

# implement stuff to only run this on finished stuff
set_download_dir_by_label(){
    echo "Usage: ${0##*/} [--move,--find,-c,-C,-w] [destination (default: ${ROOT_DIR}/<label>)]"
    flag="${1:---move}"
    ROOT_DIR="${2:-$ROOT_DIR}"
    for label in "${LABELS[@]}"; do
        dest="${ROOT_DIR%/}/${label}"
        mkdir -p "$dest"
        echo "transmission-remote -F l:${label} ${flag} ${dest}"
        transmission-remote -F "l:${label}" "$flag" "$dest"
    done
}

[[ $# -lt 1 ]] && display_help
action="$1"; shift 1
case "${action,,}" in
    label) label_torrents;;
    set_download_dir) set_download_dir_by_label "$@";;
    *) error "$action" ;;
esac
