#!/usr/bin/env bash
set -euo pipefail # bash strict mode
curl https://api.audd.io/ \
    -F url="$1" \
    -F return='apple_music,spotify' \
    -F api_token='test'
