#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# https://github.com/mmhobi7/xwinwrap
# https://github.com/ujjwal96/xwinwrap

default_path_left='/mnt/melchior-3/Pictures/Wallpapers/Video/left*'
default_path_right='/mnt/melchior-3/Pictures/Wallpapers/Video/right*'

killall xwinwrap
sleep 0.3

xwinwrap -g 2560x1440+0+753 -ni -s -nf -b -un -ov -fdt -argb -- mpv --mute=yes --video-aspect-override=16:9 --no-audio --no-osc --no-osd-bar --quiet --screen=0 --geometry=2560x1440+0+753 -wid WID --loop --shuffle $path_left &

xwinwrap -g 1440x2560+2560+0 -ni -s -nf -b -un -ov -fdt -argb -- mpv --mute=yes --panscan=1 --no-audio --no-osc --no-osd-bar --quiet --screen=0 --geometry=1440x2560+2560+0 -wid WID --loop --shuffle $path_right &
