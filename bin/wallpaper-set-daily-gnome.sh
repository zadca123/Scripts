#!/usr/bin/env bash
set -euo pipefail # bash strict mode

wget -o /tmp/wallpaper.jpg "http://www.bing.com/$(wget -q -o- https://binged.it/2zbutyc | sed -e 's/<[^>]*>//g' | cut -d / -f2 | cut -d \& -f1)"
gsettings set org.gnome.desktop.background picture-uri file:////tmp/wallpaper.jpg
