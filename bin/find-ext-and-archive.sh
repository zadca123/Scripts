#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# find -type f -size -5M -exec tar cavf archive.tar.gz {} +
find -type f -name "*.${@}$" -print0 | tar cavf archive.tar.gz --null -T -
