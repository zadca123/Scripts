#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# https://gist.github.com/mberrueta/d2234142c639de1fc6b0f2192228bcc1
# NOTE: If you're using Wayland, you might want to consider compiling Emacs 29 with the '--with-pgtk' option as well.

DATE="$(date +%F)"
VERSION_CALCULATED="$(( $(date +%y) + 5 ))"
VERSION="${1:-$VERSION_CALCULATED}"
BRANCH="emacs-${VERSION}"
BUILD_DIR="/tmp/${BRANCH}_${DATE}"
INSTALL_DIR="${HOME}/.local/${BRANCH}"
JOBS="$(nproc --ignore 4)" # do not flood the cpu

sudo apt-get -y build-dep emacs
sudo apt-get -y install libtree-sitter-dev

# clone if not such dir
[[ ! -d "$BUILD_DIR" ]] && git clone --depth 1 --branch "$BRANCH" git://git.savannah.gnu.org/emacs.git "$BUILD_DIR"
cd "$BUILD_DIR"

./autogen.sh
# ./configure --prefix="$INSTALL_DIR" --without-compress-install --with-native-compilation --with-json --with-mailutils --with-tree-sitter --with-pgtk CC=gcc-12
./configure --prefix="$INSTALL_DIR" --without-compress-install --with-native-compilation --with-mailutils --with-tree-sitter CC=gcc-12
make -j "$JOBS"
make install

ln -fs "${INSTALL_DIR}/bin/emacs"* "/usr/local/bin/" # symlinking emacs, emacsclient
rsync -ruhvP "${INSTALL_DIR}/share/"* "/usr/local/share/"

# Uninstallation
# --------------

# Change directory to the folder containing the source and perform the following steps:

# $ sudo make uninstall
# $ make clean
# $ make distclean
