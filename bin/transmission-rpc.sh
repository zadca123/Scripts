#!/usr/bin/env bash
set -euo pipefail

display_help() {
    echo "Usage: ${0##*/} [arg/s]"
    echo ""
    exit 2
}
[[ $# -lt 1 ]] && display_help

TRANSMISSION_HOST="localhost"
TRANSMISSION_PORT="9091"
TRANSMISSION_RPC_URL="http://$TRANSMISSION_HOST:$TRANSMISSION_PORT/transmission/rpc"
# RPC_PASSWORD=""


SESSION_ID="$(curl -X POST -H "Content-Type: application/json" -d '{"method": "session-get"}' $TRANSMISSION_RPC_URL | sed -n 's/.*X-Transmission-Session-Id: \([^<]*\)<.*/\1/p')"
curl -X POST -H "Content-Type: application/json" -H "X-Transmission-Session-Id: $SESSION_ID" -d '{"method": "session-get"}' "$TRANSMISSION_RPC_URL"

# curl -X POST -H "Content-Type: application/json" -d '{"method": "session-get"}' $TRANSMISSION_RPC_URL
