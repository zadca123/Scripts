#!/usr/bin/env bash
set -euo pipefail # bash strict mode

url="https://freetubeapp.io/#download"
download_link=$(curl -Ss "$url" | grep -E 'amd64.*\.deb' | cut -d\" -f2)

filename="$(mktemp --dry-run 'freetube.XXXXX').deb"
wget -c "$download_link" -O "$filename"
sudo dpkg -i "$filename"
