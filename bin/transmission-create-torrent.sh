#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# trackers='udp://tracker.openbittorrent.com:80 http://nyaa.tracker.wf:7777/announce'
# for i in $trackers;do
# done

display_help(){
    echo "Usage: ${0##*/} [file/s]"
    exit 2;
}

while getopts 'n:t:h' opt; do
    case "$opt" in
        n) name="$OPTARG" ;;
        t) trackers+=(-t "$OPTARG") ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

[[ $# -lt 1 ]] && display_help
[[ -z $name ]] && name="${1%.*}.torrent"

url=$(curl 'https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt')
for tracker in $url; do
    # echo $tracker
    trackers+=(-t "$tracker")
done
trackers+=(-t 'udp://tracker.openbittorrent.com:80' -t 'http://nyaa.tracker.wf:7777/announce')
# echo "${trackers[@]}"

transmission-create \
    -o "$name" \
    "${trackers[@]}" \
    "$@"
