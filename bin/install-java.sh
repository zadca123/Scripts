#!/usr/bin/env bash
set -euo pipefail # bash strict mode

version=17
sudo apt-get install "openjdk-${version}-jdk"
