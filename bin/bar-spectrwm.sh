#!/usr/bin/env bash
set -euo pipefail # bash strict mode
SLEEP_SEC=3
while :; do
    echo " +@bg=5; $(bar-cmus.sh) +@bg=0; +@bg=6; $(bar-volume.sh) +@bg=0; +@bg=7; $(bar-cpu.sh) +@bg=0; +@bg=8; $(bar-memory.sh) +@bg=0; +@bg=4; $(bar-battery.sh) +@bg=0;"
    sleep $SLEEP_SEC
done
