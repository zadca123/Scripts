#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [extension-to-convert] [file/s]"
    exit 2
}

ext="$1"
[[ $# -lt 2 || -f $ext ]] && display_help
shift
for img in "$@";do
    # tesseract "$img" "${img%.*}.${ext}"
    tesseract "$img" "${img%.*}"
    mv "${img%.*}.txt" "${img%.*}.${ext}"
done
