#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} -b ${start_time} -e ${end_time[1]} [input-media-file/s]"
    exit 2
}

convert_int2time(){
    # date -u -d@$(( $1 * 10 * 60 )) +'%H:%M:%S'
    date -u -d@"$1" +'%H:%M:%S'
}

start_time='00:00:05'
end_time=(-t '00:00:10')
while getopts 'b:e:d:h' opt; do
    case "$opt" in
        b)start_time="${OPTARG}" ;;
        e)end_time=(-to "${OPTARG}") ;;
        d)end_time=(-t "${OPTARG}") ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1)) # next parameters without flags

infile="$1"
filetype=$(file --mime-type -b "$infile" | cut -d/ -f1)
# cheks if nessesary variables are correctly specified
[[ $filetype != 'video' || -z $start_time || -z ${end_time[1]} || $# -ne 1 ]] && display_help

outfile="${infile%.*}_clip-begin_${start_time}_clip-dur_${end_time[1]}.${infile##*.}"
ffmpeg -y -i "$infile" -ss "$start_time" "${end_time[@]}" -c copy "$outfile"

# ff(){
#     ffmpeg -y \
    #         -ss "$start_time" \
    #         -i "$1" \
    #         "${end_time[@]}" \
    #         -c copy \
    #         "$2"
# }
# ff "$infile" "$outfile"
# # or
# ffmpeg -y -ss "$start_time" -i "$infile" "${end_time[@]}" -c copy "$outfile"
