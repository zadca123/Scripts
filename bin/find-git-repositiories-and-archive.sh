#!/usr/bin/env bash
set -euo pipefail # bash strict mode
dt=$(date +'%Y-%m-%d')
repo="repos"
ext="${1:-py}"

find . -type d -name ".git" -print0 | xargs --null -I{} git -C {} remote get-url origin | sort | uniq > "$repo"
find . -type f -mtime -7 -size -12k -name "*.${ext}" -print0 | tar -cavf "${ext}_${dt}.tar.${2:-xz}" --null -T - "$repo"
rm "$repo"
