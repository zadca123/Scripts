#!/usr/bin/env bash
set -euo pipefail # bash strict mode

declare -A command=([path]="$1" [action]="add" [x]="0" [y]="0" )
