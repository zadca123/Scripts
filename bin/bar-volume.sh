#!/usr/bin/env bash
set -euo pipefail # bash strict mode
#vol=`amixer get Master | awk -F'[][]' 'END{ print $2" "$4 }' | sed 's/on://g'`
# vol=$(pamixer --get-volume)
# amixer -c 0 set Master "$@" > /dev/null
vol="$(amixer -c 0 get Master | tail -1 | awk '{print $4}' | sed 's/[^0-9]*//g')"
# vol=$(pactl list sinks | grep "RUNNING\|IDLE" -A 9 | grep "Volume: " | cut -d/ -f2 | tr -d '% ')
echo -e "VOL:${vol}%"
