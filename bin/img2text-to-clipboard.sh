#!/usr/bin/env bash
set -euo pipefail # bash strict mode

img="/tmp/${0##*/}.png"

xclip -selection clipboard -t image/png -o > "$img"

# Convert the image to text using Tesseract OCR
text=$(tesseract "$img" -)
# text=$(tesseract -l eng "$img" -)

# get rid of ^L lines
lines=$(printf "%s" "$text" | wc -l)

# # Copy the text to clipboard
echo "$text"  | head -n "$lines" | xclip -sel clip
