#!/usr/bin/env bash
set -euo pipefail # bash strict mode

SEP=":" # single character
SCRIPT_NAME="${0##*/}"
DATE="$(date +%F_%s)"
DIR="/tmp/${SCRIPT_NAME}/${DATE}"

display_help() {
    cat <<EOF
Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>
Dependencies: diff
Examples:
- ${SCRIPT_NAME} "ls ~/Desktop" ${SEP} "ls ~/Downloads/"
EOF
    exit 2
}


[[ $# -lt 3 ]] && display_help

mkdir -p "$DIR"
mapfile -d "$SEP" -t commands <<< "$*"

for i in "${!commands[@]}"; do
    arg="${commands[$i]}"
    name="command_$((i+1))"
    bash -c "$arg" > "${DIR}/${name}" 2> "${DIR}/${name}.err" || true
done

diff --color "${DIR}/command_1" "${DIR}/command_2"
