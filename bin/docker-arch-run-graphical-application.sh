#!/usr/bin/env bash
set -euo pipefail # bash strict mode
application="${1:-emacs}"
package="${2:-$application}"

docker build -t "$application" - << __EOF__
FROM archlinux
RUN pacman -Syyuu --noconfirm "$package"
ENV DISPLAY :0
COPY . .
CMD "$application"
__EOF__

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
docker run -ti -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH -e XAUTHORITY=$XAUTH "$application"
