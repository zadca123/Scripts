#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# Create new frame (if there isnt one or no file is specified)
if [[ -z $(pgrep -f emacsclient.\*-c) ]] || [[ $1 = "" ]]; then
    setsid -f emacsclient -a '' -c "$@" > /dev/null 2>&1
else
    setsid -f emacsclient -a '' "$@" > /dev/null 2>&1
fi
