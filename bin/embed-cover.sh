#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# if [ "$#" -ge 2 ];then
#     for x in "${@:2}";do
#         cp "$x" "new_${x}"
#         # ffmpeg -i "$x" -i "$1" -map 0 -map 1 -c copy -disposition:v:1 attached_pic "new_${x}"
#         ffmpeg -i "$x" -i "$1" -map 0 -map 1 -c copy -disposition:v:1 attached_pic "new_${x}"
#     done
# else
#     echo Usage: "${0##*/}" '[cover/image] [audio file/s]'
# fi

# defaults
cover=$(find ./ -type f -size -2M | grep -i "cover\|artwork\|front" | head -n 1 )
while getopts ':c:h' opt; do
    case "$opt" in
        c) cover="$OPTARG" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

[[ "$#" -lt 1 || ! -f $cover ]] && display_help

for infile in "$@"; do
    ext="${infile##*.}"
    outfile="${infile%.*}_cover-embedded.${ext}"

    [[ $ext == 'webm' ]] && \
        ffmpeg -i "$infile" -loop 1 -i "$cover" -r 1 "$outfile" && \
        continue

    ffmpeg -i "$infile" -i "$cover" -map 0:a -map 1 -codec copy \
        -metadata:s:v title="Album cover" -metadata:s:v \
        comment="Cover (front)" -disposition:v attached_pic \
        "$outfile"
done

# # some guy from 4chan said...
# ffmpeg -i INPUT -c:v libvpx -crf 4 -b:v 1500K -vf scale=320:-1 -an output.webm
