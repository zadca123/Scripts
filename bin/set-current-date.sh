#!/usr/bin/env bash
set -euo pipefail # bash strict mode
sudo && \
    date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"
