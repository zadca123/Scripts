#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} [archive/s]"
    echo "Available extensions:"
    echo "  <zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|exe|tar.bz2|tar.gz|tar.xz>"
    exit 2
}
[[ $# -lt 1 ]] && display_help


fix_names(){
    while read -r file; do
        [[ $file !=  *[![:ascii:]]* ]] && continue # check if filename is correct

        fixed_name=$(echo "$file" | tr -cd '[:print:]')
        mv -v "$file" "$fixed_name"
    done < <(find .)
}


for archive in "$@"; do
    # archive="$(basename "$archive")" # TODO: fix absolute path problem
    dirname="${archive%.*}" # removing single extension
    dirname="${dirname//.tar/}" # removing .tar if exists
    mkdir -p "$dirname"
    pushd "$dirname"

    case "${archive,,}" in
        *.tar* | *.tgz |*.tbz2 | *.txz) tar xvf "../${archive}" ;;
        *.lzma) unlzma "../${archive}" ;;
        *.bz2) bunzip2 "../${archive}" ;;
        *.gz) gunzip "../${archive}" ;;
        *.xz) unxz "../${archive}" ;;
        *.7z | *.zip) 7z x "../${archive}" && fix_names ;;
        *.Z) uncompress "../${archive}" ;;
        *.rar) unrar x "../${archive}" ;;
        *.exe) cabextract "../${archive}" ;;
        *) display_help ;;
    esac

    popd
done
