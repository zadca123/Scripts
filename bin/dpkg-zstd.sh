#!/usr/bin/env bash
set -euo pipefail # bash strict mode
DEBPACKAGE="$1"

if [ "$1" = "" ]; then
    echo
    echo "Usage: $0 <package_name> without .deb"
    echo "Like zstd_repack.sh package_name"
    echo
    exit
fi

ar x $DEBPACKAGE.deb
zstd -d < control.tar.zst | xz > control.tar.xz
zstd -d < data.tar.zst | xz > data.tar.xz
ar -m -c -a sdsd "$DEBPACKAGE"_repacked.deb debian-binary control.tar.xz data.tar.xz
rm debian-binary control.tar.xz data.tar.xz control.tar.zst data.tar.zst
