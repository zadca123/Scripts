#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# find "$PWD" -type d -name ".git" -exec sh -c 'cd {}/.. && git status' \;
find -L "$PWD" -type d -name ".git" -exec sh -c 'cd {}/.. && git status' \;
