#!/usr/bin/env bash

mapfile -t files < <(find)
for file in "${files[@]}"; do
    [[ $file !=  *[![:ascii:]]* ]] && continue # check if filename is correct

    fixed_name=$(echo "$file" | tr -cd '[:print:]')
    mv -v "$file" "$fixed_name"
done
