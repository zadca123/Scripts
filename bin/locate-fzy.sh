#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
SCRIPT_NAME="${0##*/}"


display_help() {
    echo "Requirements: fzy locate|mlocate|plocate"
    echo "Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>"
    exit 2
}


[[ $# -lt 1 ]] && display_help


locate "*" | fzy
