#!/usr/bin/env bash
set -euo pipefail # bash strict mode

ROOT="$HOME"
DOT_DIR="${ROOT}/.config/dots" # absolute path to your git folder
SYSTEMD_DIR="${ROOT}/.config/systemd" # systemd needs to be specified
                                      # separately, if just normal
                                      # symlink files, they are
                                      # symlinked again if enabled and
                                      # will dissapear from their
                                      # directory
CMD="ln -rsv" # works for everything except systemd services
SCRIPT_NAME="${0##*/}"


display_help(){
    echo "Usage: ${SCRIPT_NAME} <flag>"
    echo "Check if ❛${DOT_DIR}❜ is yours dotfiles directory path"
    echo "No flags == DRY RUN"
    echo "-d, --dry-run == link files"
    echo "-l, --link == link files"
    echo "-f, --force == link files with overwrite"
    exit 2
}
create_dirs(){
    file="$1"
    new="${file//${DOT_DIR}/${ROOT}}"
    newDir="${new%/*}"
    mkdir -p "$newDir"
}
dry_run(){
    file="$1"
    link="${file//${DOT_DIR}/${ROOT}}"
    [[ ! -f $link ]] && echo "DRY RUN: $CMD ${file} --> ${link}"
}
symlink_systemd_dir(){
    rm -rf "$SYSTEMD_DIR"
    $CMD -- "${DOT_DIR}/.config/systemd" "$SYSTEMD_DIR"
}
symlink_file(){
    file="$1"
    create_dirs "$file"
    $CMD -- "$file" "$new"
}
symlink_force(){
    file="$1"
    create_dirs "$file"
    $CMD -f -- "$file" "$new"
}


[[ ! -d $DOT_DIR || $# -ne 1 ]] && display_help


while IFS= read -r file; do
    case "$1" in
        -d|--dry-run) dry_run "$file";;
        -l|--link) symlink_file "$file";;
        -f|--force) symlink_force "$file";;
        *) display_help;;
    esac
done < <(find "$DOT_DIR" -not -type d -not -path "*\.git*" -not -path "*/systemd/user/*")


symlink_systemd_dir


# stow --dir "$DOT_DIR" --target "$ROOT"
