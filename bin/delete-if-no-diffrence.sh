#!/usr/bin/env bash
set -euo pipefail # bash strict mode
trashBin=~/trash
mkdir -p $trashBin
dirA="/path/to/dirA/"
for file in $dirA/*; do
    fileName=${file##*/}
    diff -q <(sort "$file") <(sort /path/to/dirB/"$fileName") &&
    mv /path/to/dirB/"$fileName" $trashBin
done
