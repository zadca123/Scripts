#!/usr/bin/env bash
set -euo pipefail # bash strict mode

sudo apt-get install -y ninja-build gettext cmake unzip curl
git clone https://github.com/neovim/neovim --depth 1 --single-branch /tmp/neovim
cd /tmp/neovim
make CMAKE_BUILD_TYPE=RelWithDebInfo
sudo make install


curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim" \
     --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
nvim +PlugInstall
