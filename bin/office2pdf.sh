#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} <format-to-convert> <file/s>"
    exit 2
}

ext="${1:-}"

if [[ ! -f $ext && $# -ge 2 ]]; then
    libreoffice --headless --invisible --convert-to "$ext" "${@:2}"
else
    display_help
fi
