#!/usr/bin/env bash
set -euo pipefail # bash strict mode

require_sudo(){
    if [[ $EUID -ne 0 ]]; then
        echo "Please run as superuser! Try running:"
        echo "sudo ${0}"
        exit 2
    fi
}
require_sudo

dt=$(date +%F-%H%M%S)
file="/etc/ImageMagick-6/policy.xml"
backup="${file}.backup.${dt}"

# backup config
cp "$file" "$backup"

# filesize for converting images
old='<policy domain="resource" name="memory" value="256MiB"/>'
new='<policy domain="resource" name="memory" value="4GiB"/>'
sed -i "s#${old}#${new}#g" "$file"

# filesize for converting pdfs
old='<policy domain="resource" name="map" value="512MiB"/>'
new='<policy domain="resource" name="map" value="4GiB"/>'
sed -i "s#${old}#${new}#g" "$file"

diff --color "$backup" "$file"
