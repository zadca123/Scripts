#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$'\n\t' # Internal Field Separator, controls word splitting, default is $' \n\t'

echo "$XDG_SESSION_TYPE"
notify-send "$XDG_SESSION_TYPE"
