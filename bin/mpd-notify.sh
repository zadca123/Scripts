#!/usr/bin/env bash
set -euo pipefail # bash strict mode
msgId="291011"

if ! pidof mpd; then
    echo "mpd is not running..."
    exit 2
fi

# music_directory=$(cat ~/.config/mpd/mpd.conf | awk '/music_directory/ {print $2}' | tr -d \"\')
# abs_path=$(realpath $music_directory)
abs_path=$(realpath ~/Music)
cover-music-generate.sh "${abs_path}/$(mpc --format %file% | head -n1)" "${1:-200}"
sleep 0.2

tags=$(mpc --format 'Disc: %disc%\nGenre: %genre%\nPerformer: %performer%\nTitle: %title%\nTrack: %track%\nTime: %time%\nPosition: %position%\nId: %id%\nPrio: %prio%' | \
    sed 's/ /\t/' | column -t -s $'\t')
title='mpd-notifier'
notify-send -i "/tmp/cover.png" "$title" "$tags"
# dunstify -i "/tmp/cover.png" -r "$msgId" "$title" "$info"

# set this in moc config
#OnSongChange = /absolute/path/moc-notify.sh
#RepeatSongChange = yes
