#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [flag/s] [file/s]"
    echo "Flags:"
    echo -e "\t -h --Display help message"
    exit 2
}

[[ $# -lt 1 ]] && display_help

song_next(){
    mpc next || cmus-remote --next || mocp --next
}

song_prev(){
    mpc prev || cmus-remote --prev || mocp --prev
}

song_toggle(){
    mpc toggle || cmus-remote --pause || mocp --toggle-pause
}

song_volume(){
    mpc volume "$1" || cmus-remote --volume "$1" || mocp --volume "$1"
}

song_stop(){
    mpc stop || cmus-remote --stop || mocp --stop
}

show_notification(){
    cover_size="${1:-200}"
    if pidof mpd; then
        mpd-notify.sh "$cover_size"
    elif pidof cmus; then
        cmus-notify.sh "$cover_size"
    elif pidof mocp; then
        mocp-notify.sh "$cover_size"
    fi
}

while getopts 'nptsv:h' opt; do
    case "$opt" in
        n) song_next ;;
        p) song_prev ;;
        t) song_toggle ;;
        v) song_volume "$OPTARG" ;;
        s) song_stop;;
        h|\?|:|*) display_help ;;
    esac
done

show_notification 200
