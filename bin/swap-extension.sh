#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [old-extension] [new-extension]"
    echo "Usage: ${0##*/} [delete-this-extension-from-filename]"
    exit 2
}

[[ $# -lt 1 ]] && display_help
[[ -n $2 ]] && 2=".${2}"
find . -type f -name "*.${1}" -print0 | rename --force --null "s/\.${1}\$/${2}/g" --

# >>86204202 (You)
# What's the point in removing the extension ? anyway, no idea if this works, so first of all try this:
# find . -type f -name '*.mp3' | while IFS= read -r line; do printf 'mv -- %s %s\n' "$line" "${line%.mp3}"; done
# If it seems fine to you,
# find . -type f -name '*.mp3' | while IFS= read -r line; do mv -- "$line" "${line%.mp3}"; done

# >>86204496 (You)
# Assigning 2 ($2) does not work, unless it's zsh ... in which case, you might want to use zmv:
# autoload -Uz zmv
# zmv -n '**/*.mp3' '${f%.mp3}'
# Remove the -n option if the commands seem fine to you. for the more readable command see >>86204501
