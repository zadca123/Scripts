#!/usr/bin/env bash
set -euo pipefail

SCRIPT_NAME="${0##*/}"
display_help() {
    cat <<EOF
Usage: ${SCRIPT_NAME} <putty ssh2 key or something>
Dependencies:
Examples:
- ${SCRIPT_NAME}
- ${SCRIPT_NAME}
EOF
    exit 2
}
message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ===="
}


[[ $# -lt 1 || ! -f "$1" ]] && display_help
putty_key="$1"


ssh-keygen -i -f "$putty_key" > openssh.pub
