#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# Function to validate and calculate time until wakeup
calculate_seconds_until_wake() {
    local input_time="$1"
    wake_time=$(date -d "$input_time" +%s 2>/dev/null)
    if [[ $? -ne 0 ]]; then
        echo "Invalid time format. Please use formats like 'tomorrow 08:00' or '+10h'."
        exit 1
    fi
    current_time=$(date +%s)
    seconds_until_wake=$((wake_time - current_time))
    if [[ $seconds_until_wake -le 0 ]]; then
        echo "Specified time is in the past. Please provide a future time."
        exit 1
    fi
}

# Check if rtcwake is available
if ! command -v rtcwake &> /dev/null; then
    echo "rtcwake is not installed. Please install it and try again."
    exit 1
fi

# Prompt user for wake-up time
WAKEUP_TIME="tomorrow 08:00"
read -erp "Enter the time to wake up: " -i "$WAKEUP_TIME" WAKEUP_TIME
calculate_seconds_until_wake "$WAKEUP_TIME"

# Confirm and execute shutdown
echo "Scheduling shutdown until $WAKEUP_TIME."
echo "The system will power on in $seconds_until_wake seconds."
read -p "Press Enter to proceed or Ctrl+C to cancel..."

# Execute rtcwake command
sudo rtcwake -m off -s "$seconds_until_wake"

# Optional: Print a message after the system shuts down
echo "The system is shutting down and will wake up at $WAKEUP_TIME."
