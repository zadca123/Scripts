#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [ext] [command]"
    exit 2
}

[[ $# -lt 2 ]] && display_help

find -type f -name "*.${1}" -print0 -exec "${@:2}" {} +
