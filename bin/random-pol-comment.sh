#!/usr/bin/env bash
set -euo pipefail # bash strict mode
curl https://a.4cdn.org/pol/catalog.json \
    | jq '.[0].threads[2].last_replies[0].com' \
    | sed 's/<.*>//gm'
