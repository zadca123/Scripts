#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

SCRIPT_NAME="${0##*/}"


display_help() {
    echo "Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>"
    echo "Dependencies: export"
    echo "Example:"
    echo "${SCRIPT_NAME} ./some/dir"
    echo "${SCRIPT_NAME} /opt/jetbrains/bin"
    exit 2
}


[[ $# -lt 1 ]] && display_help


for dir in "$@"; do
    dir="$(realpath "$dir")"
    PATH="${PATH}:${dir}"
done

export PATH
