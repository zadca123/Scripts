#!/usr/bin/env bash
set -euo pipefail # bash strict mode

DATE="$(date +%F_%H%M%S)"
SCRIPT_NAME="${0##*/}"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME%%.sh}_${DATE}.log"

display_help() {
    error_message="$*"
    [[ -n ${error_message:-} ]] && echo -e "Error: ${error_message}\n"

    cat <<EOF
Usage: ${SCRIPT_NAME} <action> <package/s>

Works with: pacman, apt, zypper, dnf

OPTIONS:
  -u, --update        Update packages
  -i, --install       Install packages
  -r, --remove        Remove packages
  -c, --clear-cache   Clear package cache
  -s, --search        Search for packages
  -l, --list          List installed packages
  -d, --depends       List package dependencies
  -h, --help          Display this help message

Dependencies: bash
Examples:
- \$ ${SCRIPT_NAME} -i tmux
- \$ ${SCRIPT_NAME} -r tmux
- \$ ${SCRIPT_NAME} -c
EOF
    exit 2
}

[[ $# -lt 1 ]] && display_help
mkdir -pv "$LOG_DIR"
ln -rsf "$LOG_DIR" "$HOME"


zypper_manager(){
    if [ -e /etc/os-release ]; then
        . /etc/os-release
    else
        . /usr/lib/os-release
    fi

    local action="$1"
    shift
    case "${action,,}" in
        u | update) sudo zypper ref
                    if [[ $ID = "opensuse-tumbleweed" ]]; then
                        sudo zypper dup
                    elif [[ $ID = "opensuse-leap" ]]; then
                        sudo zypper up
                    fi ;;
        i | install) sudo zypper in -- "$@";;
        r | remove) sudo zypper re -- "$@";;
        c | clear) zypper clean -a;;
        s | search) zypper se -- "$@";;
        l | list) zypper lp -- "$@";;
        d | depends) rpm -qR -- "$@";;
        h|\?|:|*) display_help ;;
    esac
}

apt_manager(){
    local action="$1"
    shift
    case "${action,,}" in
        u | update) sudo apt update && sudo apt upgrade;;
        i | install) sudo apt install -- "$@";;
        r | remove) sudo apt autopurge -- "$@";;
        c | clear) sudo apt autoremove && sudo apt clean && sudo apt autoclean;;
        s | search) apt search -- "$@";;
        l | list) apt list -- "$@";;
        d | depends) apt-cache depends -- "$@";;
        h|\?|:|*) display_help ;;
    esac
}

pacman_manager(){
    local action="$1"
    shift
    case "${action,,}" in
        u | update) sudo pacman -Syu --needed;;
        i | install) sudo pacman -Syu --needed -- "$@";;
        r | remove) sudo pacman -Rncssu -- "$@";;
        c | clear) sudo pacman -Scc;;
        s | search) pacman -Qs -- "$@";;
        l | list) pacman -Ss -- "$@";;
        d | depends) pacman -Q -- "$@";;
        h|\?|:|*) display_help ;;
    esac
}

dnf_manager(){
    local action="$1"
    shift
    case "${action,,}" in
        u | update) sudo dnf upgrade --refresh;;
        i | install) sudo dnf install -- "$@";;
        r | remove) sudo dnf remove -- "$@";;
        c | clear) sudo dnf clean all;;
        s | search) dnf search all -- "$@";;
        l | list) dnf list -- "$@";;
        d | depends) rpm -qR -- "$@";;
        h|\?|:|*) display_help ;;
    esac
}

flatpak_manager(){
    local action="$1"
    shift
    case "${action,,}" in
        u | update) flatpak update -y;;
        i | install) flatpak install -y "$@";;
        r | remove) flatpak uninstall -y "$@";;
        c | clear) flatpak uninstall --unused -y;;
        s | search) flatpak search "$@";;
        l | list) flatpak list;;
        d | depends) flatpak info --show-deps "$@";;
        h|\?|:|*) display_help;;
    esac
}

if command -v pacman; then
    pacman_manager "$@"
elif command -v apt; then
    apt_manager "$@"
elif command -v zypper; then
    zypper_manager "$@"
elif command -v dnf; then
    dnf_manager "$@"
else
    display_help "package manager not supported!@!@!@"
fi

read -erp "Try flatpak? (yes/no): " -i "no" ans
if [[ "$ans" =~ ^(yes|y)$ ]]; then
    flatpak_manager "$@"
fi
