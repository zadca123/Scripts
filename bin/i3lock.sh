#!/usr/bin/env bash
set -euo pipefail # bash strict mode
##CHECK IF I3LOCK IS RUNNING AND SUSPEND AFTER 5 MINS
#while true; do
#    lock=$( ps aux | grep '[i3]lock')
#    if [ -n "$lock" ]; then
#        sleep 5m
#        systemctl suspend
#    fi
#    sleep 5
#done

location=${WALLPAPERS:-"${HOME}/Pictures"}
find "$location" -type f -name "*.png" -print0 | shuf | head -zn1 | xargs --null -- i3lock -n -i

# width=$(xwininfo -root | awk 'FNR == 8 {print $2}')
# height=$(xwininfo -root | awk 'FNR == 9 {print $2}')
# resolution="${width}x${height}"
# # if ratio is 21:9, then let image be a locker wallpaper
# ratio=$(( width / height ))

# images=$(find "$location" -type f -name "*.png" -print0 | shuf)
# for img in $images; do

#     # read width height < <(identify -format "%w %h" "${img}")
#     width=$(identify -format "%w" "$img")
#     height=$(identify -format "%h" "$img")
#     # ratio=$(( width / height ))
#     echo $width
#     echo $height
#     # if [[ $ratio -gt 2 ]]; then
#     #     echo 0
#     # else
#     #     echo 1
#     # fi

#     # echo "$img"
# done

# # fdfind -t f -e png -1 -0 --absolute-path --full-path "$location" | xargs --null -I {} i3lock -n -i {}
# # find "$location" -type f -name "*.png" -print0 | shuf | head -zn1 | xargs --null -- i3lock -n -i

# # can use imagemagick for this
# # https://legacy.imagemagick.org/Usage/blur/
