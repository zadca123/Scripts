#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# high="$1" # High image (full-size original view)
# low="$2" # Low image (thumbnail) (should be the same size)
# output="output.png"
# [ ! -z "$3" ] && output="$3" # Output image
# size=$(convert "$high" -format "%wx%h" info:)
# convert "$high" -alpha off +level 3.5%,100% -gamma 20 high_gamma.png
# low_gamma="-alpha off -gamma 0.8 +level 0%,77%"
# convert \( "$low" $low_gamma \) high_gamma.png \
    #            -compose Mathematics -define compose:args='0,-0.33,1,0.33' \
    #            -composite low_adjusted.png
# convert low_adjusted.png high_gamma.png -size $size pattern:gray25 -composite \
    #         -set gamma 0.022727 -define png:include-chunk=none,gAMA "$output"
# rm high_gamma.png low_adjusted.png

# this autoscales image or some shit (better than the above)
[ -z "$1" -o -z "$2" ] && echo "Need 2 input images." && exit 1
[ -z "$3" ] && out="out.png" || out="$3"

ffmpeg \
    -loglevel 'error' \
    -y \
    -i "$1" \
    -i "$2" \
    -filter_complex \
    "`echo \
    'color=c=black[mask];' \
    '[mask][0:v]scale2ref[mask][main];' \
    '[1:v][main]scale2ref[scnd][main];' \
    '[mask]drawgrid=w=2:h=2:c=white,format=rgb24[mask];' \
    '[main]colorlevels=romin=0.01:gomin=0.01:bomin=0.01' \
    ':romax=0.21:gomax=0.21:bomax=0.21,eq=gamma=10.0,eq=gamma=1.4' \
    ',format=rgb24,split[main][temp];' \
    '[scnd]colorlevels=romin=0.0:gomin=0.0:bomin=0.0' \
    ':romax=0.8:gomax=0.8:bomax=0.8,eq=gamma=0.7,format=rgb24[scnd];' \
    '[temp][scnd]blend=all_expr='"'B - 0.33*A + 84'"',format=rgb24[scnd];' \
    '[main][scnd][mask]maskedmerge[vout]'`" \
    -frames:v 1 \
    -c:v png \
    -f image2 \
    -map '[vout]' \
    - | \
    convert \
    - \
    -set 'gamma' '0.027' \
    -define 'png:include-chunk=none,gAMA' \
    "$out"
