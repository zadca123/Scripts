#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# recover max native resolution
CURRES=$(xrandr | sed -n 3p  | awk  '{print $1}')
FULLW="$(cut -d'x' -f1 <<< $CURRES)"
FULLH="$(cut -d'x' -f2 <<< $CURRES)"

# taskbar pixel height
TASKH=25 # hardcoded

# active window id
# ID="-r :ACTIVE:" # does not work as consistently as the call to xdotool
ID="-i -r `xdotool getwindowfocus`"

# disable maximized attribute
wmctrl $ID -b remove,maximized_vert
wmctrl $ID -b remove,maximized_horz

case "$1" in
    w)
        # tile left
        W=$(($FULLW / 2))
        H=$(($FULLH - $TASKH))
        X=0
        Y=0
        ;;
    e)
        # tile right
        W=$(($FULLW / 2))
        H=$(($FULLH - $TASKH))
        X=$W
        Y=0
        ;;
    n)
        # tile top
        W=$FULLW
        H=$((($FULLH - $TASKH) / 2))
        X=0
        Y=0
        ;;
    s)
        # tile bottom
        W=$FULLW
        H=$((($FULLH - $TASKH) / 2))
        X=0
        Y=$H
        ;;
esac

# resize
wmctrl $ID -e 0,$X,$Y,$W,$H
