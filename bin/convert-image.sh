#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [png,jpeg,pdf,etc...] [file/s]"
    exit 2
}

ext="$1"
[[ $# -lt 2 || -f $ext ]] && display_help

shift
for img in "$@";do
    filetype=$(file --mime-type -b "$img" | cut -d/ -f1)
    [[ ! -f $img  || ! $filetype == 'image' ]] && echo "${img} is NOT A IMAGE, skipping..." && continue
    convert "$img" "${img%.*}.${ext}"
done
