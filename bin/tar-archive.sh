#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [gzip,xz,bzip2] [file/s]"
    exit 2
}

[[ $# -lt 2 ]] && display_help

cmd="$1"
archive="${2}_$(date '+%Y-%m-%d-%H%M%S').tar.${cmd}"

tar -cvf - ${@:2} | "$cmd" -9 > "$archive"
