#!/usr/bin/env bash
set -euo pipefail # bash strict mode

input="${1:-$PWD}"
basename="${input##*/}"
date="$(date +'%F')"
watched_movies_file="${HOME}/Documents/movies-watched.txt"

echo "${date} ${basename}" >> "$watched_movies_file"
sort -u "$watched_movies_file" -o "$watched_movies_file"
echo "Added file to a watched list"
touch WATCHED FINISHED COMPLETED
