#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# https://github.com/dexpota/kitty-themes

path="${DOTFILES}/.config/kitty/kitty-themes/themes"
# for theme in $(find . -type f -name "*.conf" | shuf); do
random=$(find "${path}" -type f -name "*.conf" | shuf)

# for theme in $random; do
#     ln -sf "$theme" "${HOME}/.config/kitty/theme.conf"
#     sleep 1h
# done

ln -sf "$(echo "$random" | head -n1)" "${HOME}/.config/kitty/theme.conf"
kitty
