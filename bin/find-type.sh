#!/usr/bin/env bash
set -euo pipefail # bash strict mode

mimetypes=$(sed -E 's/\/.*//g; /^$/d; /^#/d' /etc/mime.types | uniq)
display_help(){
    echo "Usage: ${0##*/} [mimetype]"
    echo "Available mimetypes:"
    echo "$mimetypes"
    exit 2
}

[[ $# -lt 1 ]] && display_help

ext=$(sed -E "/^${1}/!d; s/^[^ \t]+[ \t]*//g; /^$/d; s/ /\n/g" /etc/mime.types | sed -Ez 's/\n$//; s/\n/\\|/g; s/(.*)/\.*\\.\\(\1\\)\n/')
find -type f -regex "$ext" "${@:2}" 2>/dev/null
