#!/usr/bin/env bash
set -euo pipefail # bash strict mode
! [[ -f /usr/bin/acpi ]] && exit

while :; do
    batt=$(acpi | tail -n1 | cut -d' ' -f4 | tr -d '%,')
    stat=$(acpi | tail -n1 | cut -d' ' -f3 | tr -d ',')

    if [[ $batt -lt 15 && $stat != "Charging" ]]; then
        notify-send -u critical "$stat" "Current battery level at ${batt}%"
    elif [[ $batt -gt 85 && $stat == "Charging" ]]; then
        notify-send -u critical "$stat" "Current battery level at ${batt}%"
    elif [[ $batt -lt 3 && $stat != "Charging" ]]; then
        systemctl poweroff
    fi

    sleep 5m
done
