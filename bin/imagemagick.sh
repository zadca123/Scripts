#!/usr/bin/env bash
set -euo pipefail # bash strict mode

dt="$(date +'%Y-%m-%d-%H%M%S')"
val=1
ext="png"
operations=(convert chop cut resize add_border merge_horizontally merge_vertically create_gif)
gravities=(North South East West)


list_items(){
    arr=("$@")
    for((i=0;i<${#arr[@]};i++)); do
        echo "    ${i} --> ${arr[${i}]}"
    done
}


display_help(){
    echo "Usage: ${0##*/} [operation] [file/s]"
    echo "Flags and default values:"
    echo "-e [image extension] --> set output extension (default: ${ext})"
    echo "-v [value digit] --> sets value to apply on images if needed (default: ${val})"
    echo "-o [operation digit] --> available operations"
    list_items "${operations[@]}"
    echo "-g [gravity digit] --> operation of cutting image"
    list_items "${gravities[@]}"
    exit 2
}


[[ $# -lt 1 ]] && display_help


while getopts 'v:e:o:g:h' opt; do
    case $opt in
        v) val="$OPTARG" ;;
        e) ext="$OPTARG" ;;
        o) operation="${operations[${OPTARG}]}" ;;
        g) gravity="${gravities[${OPTARG}]}" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))


[[ $# -lt 1 || -z $operation ]] && display_help


input="$@"
identify "$input"


chop_func(){
    output="${operation}_${val}_${1}"
    convert "$1" -gravity "$gravity" -chop "x${val}" +repage "$output"
}
cut_func(){
    echo not finished
}
resize_func(){
    input="$1"
    output="${operation}_${val}_${1}"
    convert -adaptive-resize "${val}" -format "${ext}" -quality 80 "$input" "$output"
}
add_border_func(){
    output="${operation}_${val}x${val}_${1}"
    convert "$1" -bordercolor black -border "${val}%x${val}%" "$output"
}
merge_vertically_func(){
    output="${operation}_${dt}.${ext}"
    convert -append "$@" "$output"
}
merge_horizontally_func(){
    output="${operation}_${dt}.${ext}"
    convert +append "$@" "$output"
}
create_gif_func(){
    output="${operation}_${dt}.gif"
    convert -delay "$val" -loop 0 "$@" "$output"
}
convert_func(){
    output="${operation}_${dt}.${ext}"
    convert "$@" "$output"
}


case ${operation,,} in
    convert) convert_func "$input";;
    chop) chop_func "$input";;
    cut) cut_func "$input" ;;
    resize) resize_func "$input";;
    add_border) add_border_func "$input";;
    merge_vertically)  merge_vertically_func "$input";;
    merge_horizontally) merge_horizontally_func "$input";;
    create_gif)  create_gif_func "$input";;
    h|\?|:|*) display_help ;;
esac
identify "$output"
