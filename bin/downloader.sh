#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# TODO: Scripts stops working after some time( long one) and needs restart, Fix this.
# Automatifally downloads files in clipboard
memes="${HOME}/Documents/memes"
misc="${HOME}/Downloads/misc"
mkdir -p "$memes" "$misc"

# sep="/"
delay='3s'
while :; do
    url=$(xclip -selection clipboard -o)
    [[ $url == $old || $url != 'http'* ]] && sleep "$delay" && continue

    case "$url" in
        *.4cdn.org*[0-9].*) dir="$memes" ;;
        *.png | *.jpg | *.jpeg | *.gif | *.webm | *.mp4 | *.pdf | *.zip | *.7z | *.tar* | *.rar) dir="$misc" ;;
        *) sleep "$delay" && continue ;;
    esac

    # aria2c --allow-overwrite -d "$dir" "$url"
    $(detach.sh) \
        wget -nc -P "$dir" "$url" && \
        recent="${dir}/${url##*/}" && \
        notify-send -i "$recent" "Saving as ${recent}"

    old="$url"
done
