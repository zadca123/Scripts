#!/usr/bin/env bash
set -euo pipefail # bash strict mode
[[ "$#" -eq 2 ]] && \
    mkdir -p "$2" && \
    rsync -ahzAxmivP --mkpath "$1" "$2" && \
    exit 0

echo Usage: "$0" '[dir/file-to-backup] [backup-location]'
