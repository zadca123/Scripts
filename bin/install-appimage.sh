#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$'\n\t' # Internal Field Separator, controls word splitting, default is $' \n\t'

appimage="$1"
chmod +x "$appimage"
mv "$appimage" "${HOME}/.local/bin/"
