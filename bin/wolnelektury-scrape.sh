#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$'\n\t' # Internal Field Separator, controls word splitting, default is $' \n\t'

api="https://wolnelektury.pl/api/books/"
output_dir="${HOME}/Documents/wolnelektury/"

curl -sS "$api" \
    | jq -r '.[]["href"]' \
    | xargs curl -sS -- \
    | jq -r '{epub,mobi,pdf,html,txt,fb2,xml} | values[]' \
    | xargs wget -c -P "$output_dir" --

curl https://wolnelektury.pl/api/audiobooks/ | jq '.[]["href"]' | head -n1 | xargs curl -- | jq -r '.["media"][]["url"]' | grep ".ogg"
