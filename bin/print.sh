#!/usr/bin/env bash
set -euo pipefail # bash strict mode
numberOfPages=$(pdfinfo $1 | grep ^Pages | tr -s ' ' | cut -d' ' -f2)

lp "$1" -P ""
