#! /bin/bash
set -euo pipefail # bash strict mode

mem="$(free -h | awk '/^Mem:/ {print $3 "/" $2}')"
echo "${mem} RAM"
