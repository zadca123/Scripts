#!/usr/bin/env bash
set -euo pipefail # bash strict mode

temp="/tmp/temp.txt"
for file ; do
    uniq "$file" > "$temp"
    # awk '!seen[$0]++' "$file" > "$temp"
    cp "$temp" "$file"
done
