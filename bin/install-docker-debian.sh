#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# NEW WAY
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update -y
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo apt-get update -y

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
# sudo docker run hello-world

sudo groupadd docker
sudo usermod -aG docker "$USER"
sudo gpasswd -a "$USER" docker
newgrp docker
sudo systemctl enable --now docker
