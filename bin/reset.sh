#!/usr/bin/env bash
set -euo pipefail # bash strict mode
killall "$@" || "$@"

exit
