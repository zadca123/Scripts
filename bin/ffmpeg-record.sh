#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# dependencies: xwininfo, ffmpeg, slop
display_help(){
    echo "Usage: ${0##*/}"
    echo "Captures by default fullscreen"
    echo "Flags:"
    echo "-f --focused window"
    echo "-s --selection"
    echo "-e --extension (default: mkv)"
    echo "-d --duration"
    echo "-F --framerate"
    exit 2
}

countdown(){
    for i in {3..1}; do
        echo "${i}..."
        sleep 1
    done
}

select_fullscreen(){
    info=$(xwininfo -root)
    width=$(echo "$info" | awk 'FNR == 8 {print $2}')
    height=$(echo "$info" | awk 'FNR == 9 {print $2}')
    geometry=(-video_size "${width}x${height}" -i "${DISPLAY}.0+0+0")
}

select_selection(){
    notify-send "Draw frame to record"
    geometry=($(slop -f "-video_size %wx%h -i ${DISPLAY}.0+%x,%y"))
}

select_frame(){
    notify-send "Select frame to record"
    info=$(xwininfo -frame)
    width=$(echo "$info" | awk 'FNR == 12 {print $2}')
    height=$(echo "$info" | awk 'FNR == 13 {print $2}')
    x=$(echo "$info" | awk 'FNR == 8 {print $4}')
    y=$(echo "$info" | awk 'FNR == 9 {print $4}')
    geometry=(-video_size "${width}x${height}" -i "${DISPLAY}.0+${x},${y}+0")
}

set_output(){
    directory="${HOME}/Documents/recordings"
    mkdir -p "$directory"
    date=$(date '+%Y-%m-%d-%H%M%S')
    output="${directory}/rec_${date}.${ext:-mkv}"
    # audio_src=$(pactl list sources short | awk '/output/ && /analog-stereo/ {print $1}') # it requires dependency to install now
    audio_src=$(pw-cli list-objects Node | grep output -B 9 | grep object.serial | tr -d ' "' | cut -d '=' -f2)
}

record(){
    ffmpeg \
        "${options[@]}" \
        -f x11grab \
        "${geometry[@]}" \
        -f pulse \
        -ac 2 \
        -i "$audio_src" \
        "$output"
}

while getopts ':fsd:e:F:h' opt; do
    case "$opt" in
        f) select_frame ;;
        s) select_selection ;;
        e) ext="$OPTARG" ;;
        d) options+=(-t "$OPTARG") ;;
        F) options+=(-framerate "$OPTARG") ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

[[ -z ${geometry:-} ]] && select_fullscreen

set_output
countdown
record
