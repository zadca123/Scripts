#!/usr/bin/env bash
set -euo pipefail # bash strict mode


SCRIPT_NAME="${0##*/}"


display_help() {
    echo "Usage: ${SCRIPT_NAME} <bat, chafa, audio>"
    echo "Dependencies: fzf, bat, chafa, ffplay"
    echo "Example:"
    echo "- ${SCRIPT_NAME} bat"
    echo "- ${SCRIPT_NAME} chafa"
    exit 2
}


[[ $# -lt 1 ]] && display_help


action="$1"
CMD="fzf --preview"
run_fzf(){
    rest="${*}"
    $CMD "${rest} {}"
}


case "$action" in
    bat) params=bat ;;
    chafa) params=chafa ;;
    audio) params="ffplay -nodisp -autoexit"
           # params='mpv --no-audio-display'
           ;;
    *) display_help ;;
esac

run_fzf "$params"
