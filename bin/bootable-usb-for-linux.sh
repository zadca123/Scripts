#!/usr/bin/env bash
set -euo pipefail # bash strict mode
SCRIPT_NAME="${0##*/}"


display_help() {
    echo "Usage: ${SCRIPT_NAME} <iso-file> </dev/sdX>"
    echo "Dependencies: dd"
    echo "Examples:"
    echo "- ${SCRIPT_NAME} ~/Downloads/ubuntu.iso /dev/sde"
    exit 2
}
require_sudo() {
    if [[ $EUID -ne 0 ]]; then
        echo "Please run as superuser! Try running:"
        echo "sudo ${0}"
        exit 2
    fi
}


require_sudo
[[ $# -ne 2 || -f $2 || ! -f $1 ]] && display_help


IMAGE="$1"
LABEL="$(basename "${IMAGE%.*}")"
DEVICE="$2"


set -x # enable debugging
mkfs.ext4 -L "$LABEL" "$DEVICE"
dd if="$IMAGE" of="$DEVICE" bs=1M status=progress
sync
