#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help() {
    echo "Usage: ${0##*/} [pattern]"
    echo "Dependencies: plocate or mlocate"
    echo "After installation run 'updatedb' command"
    exit 2
}
[[ $# -lt 1 ]] && display_help

grep "/media" /etc/updatedb.conf \
    && sed -i "s#/media ##g" /etc/updatedb.conf \
    && sudo updatedb

foldername="$*"
DIRS=() # Declaring DIRS as a global array

while IFS= read -r line; do
    x1=$(echo "$foldername" | tr '[:upper:]' '[:lower:]')
    x2=$(echo "${line##*/}" | tr '[:upper:]' '[:lower:]')
    if [[ -d $line && $x2 =~ $x1 ]]; then
        DIRS+=("$line")
        echo "$line"
    fi
done < <(locate --ignore-case --regex -- "$foldername")

link_to_dir(){
    if [[ ${#DIRS[@]} -eq 0 ]]; then
        echo "List is empty"
    else
        mkdir -pv "/tmp/${foldername}"
        ln -rsv "${DIRS[@]}" "/tmp/${foldername}/"
    fi
}

link_to_dir
