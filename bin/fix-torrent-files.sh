#!/usr/bin/env bash
set -euo pipefail # bash strict mode
path="${1:=PWD}"
suffix="${2:-part}"
files=$(find "$path" -type f -name "*.${suffix}")

arr=()
for file in $files; do
    arr+=("${file%.${suffix}}")
done

for file in ${arr[@]}; do
    echo "$file"
done

# for file in "${arr[@]}"; do
#     echo "$file"
# done
