#!/usr/bin/env bash
set -euo pipefail

DATE="$(date +%F_%H%M%S)"
SCRIPT_NAME="${0##*/}"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME%%.sh}_${DATE}.log"

display_help() {
    error_message="$*"
    [[ -n ${error_message:} ]] && echo -e "Error: ${error_message}\n"

    cat <<EOF
Description: Launch emacs -Q with littering enabled to not clutter emacs home
Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>
Dependencies: emacs no-littering
Examples:
- ${SCRIPT_NAME}
EOF
    exit 2
}
message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ====" | tee -a "$LOG_FILE"
}

LIB_DIR="${EMACS_CONFIG_DIR}/elpa"
mapfile -t flags < <(find "$LIB_DIR" -maxdepth 1 -type d -regex '.*\(no-litterin\|compat\).*' -printf '--directory=%p')
message "launching emacs with flags: ${flags[*]}"
emacs -Q "${flags[@]}" -l no-littering
