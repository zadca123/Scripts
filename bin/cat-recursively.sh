#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help() {
    cat <<EOF
Usage: ${0##*/} [OPTIONS] [FILES...]
Process the given files and directories.

Options:
  -j       Format output for JIRA
  -m       Format output for Markdown
  -4       Format output for 4chan
  -h       Display this help message and exit

Arguments:
  FILES...  List of files and directories to process. Directories will be searched for text files, and individual files will be processed as specified.

Examples:
  ${0##*/} -j file1.txt file2.txt
  ${0##*/} -m dir1 dir2 file3.txt
EOF
    exit 2
}

markdown_format(){
    local file="$1"
    cat <<EOF

==== ${file} ====
\`\`\`
$(< "$file")
\`\`\`
EOF
}

jira_format(){
    local file="$1"
    cat <<EOF

==== ${file} ====
{code:java}
$(< "$file")
{code}
EOF
}

4chan_format(){
    local file="$1"
    cat <<EOF

==== ${file} ====
[code]
$(< "$file")
[/code]
EOF
}

format(){
    local file="$1"
    case "${format_option:-}" in
        j) jira_format "$file" ;;
        4) 4chan_format "$file" ;;
        m|*) markdown_format "$file"  ;;
    esac
}


while getopts 'jm4h' opt; do
    case "$opt" in
        j|m|4) format_option="$opt" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

for thing in "$@"; do
    if [[ -d $thing ]]; then
        dirs+=("$thing")
    elif [[ -f $thing ]]; then
        files+=("$thing")
    else
        echo "I don't know what is ${thing}"
    fi
done
[[ ${dirs:-} ]] && mapfile -t ALL_FILES < <(find "${dirs[@]}" -type f -not \( -path '*/.git/*' -o -path '*/www/*' \) | sort)
[[ ${files:-} ]] && ALL_FILES+=("${files[@]}")

for file in "${ALL_FILES[@]}"; do
    if [[ $(file -b --mime-type "$file") =~  'text' ]]; then
        format "$file"
    fi
done
