#!/usr/bin/env bash
set -euo pipefail # bash strict mode
class=$(echo -e "$(xprop | grep "WM_CLASS\|_NET_WM_NAME\|WM_NAME")")
notify-send -t 10000 "$class"
echo "$class"
echo "$class" | xclip -sel c
