#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$' \n\t' # Internal Field Separator, controls word splitting,
# default is $' \n\t'

packages=(docker.io docker-compose docker-doc)
sudo apt install -y "${packages[@]}"
