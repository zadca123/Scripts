#!/usr/bin/env bash
set -euo pipefail # bash strict mode
sudo umount -a
sudo fsck -ARyf
