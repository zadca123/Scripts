#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# look into bracketed-paste-magic and url-quote-magic zsh features
output="$(date +%F)_%(upload_date)s_%(title)s_%(resolution)s.%(ext)s"
DIR="${HOME}/Videos/yt-dlp"
flags=(-ciw --add-metadata --embed-subs --sub-langs all --embed-thumbnail --sponsorblock-remove all)

display_help(){
    echo "Usage: ${0##*/} [flags] [url/s]"
    echo "-q, --Specify video quality to download, 144|240|360|480|..."
    echo "-a, --Download audio ONLY (default best quality possible)"
    echo "-c, --Adds channel name to filenames"
    echo "-r, --Regex pattern to search in channel"
    echo "-R, --Regex pattern to search in channel (case sensitive)"
    echo "-d, --Downloads to custom specified directory, def: ${DIR}"
    echo "-h, --Show this help message"
    echo "Examples:"
    echo "${0##*/} -q [144,240,360,720,...] youtube.com/..."
    echo "${0##*/} -a [best,worst] youtube.com/..."
    echo "${0##*/} -r 'Linux Crash Course' youtube.com/channel..."
    exit 2
}
setup_output_for(){
    vid_link="$1"
    prefix="all/"
    if grep -E --color 'playlist' <<< "$vid_link"; then
        prefix="%(playlist_title)s/%(playlist_index)s_"
    fi

    output="${DIR}/%(channel)s/${prefix}${output}"
}
set_cookies(){
    cookies="${DOTFILES}/.misc/yt-cookies.txt"
    if [[ -f $cookies ]]; then
        flags+=(--cookies "$cookies")
    else
        echo "No cookie file at ${cookies}!"
    fi
}
download(){
    clear_output="$output"
    for vid_link in "$@"; do
        set_cookies
        setup_output_for "$vid_link"
        yt-dlp "${flags[@]}" -o "$output" -- "$vid_link"
        output="$clear_output"
    done
}
rerun_if_not_downloaded(){
    if [[ -n $(find "$DIR" -type f -mtime -1 -iname "*.part") ]]; then
        "$0" "$@"
    fi
}

while getopts 'q:a:r:R:d:h' opt; do
    case $opt in
        q) flags+=(-f "bestvideo[height<=${OPTARG:-480}]+bestaudio/best[height<=${OPTARG:-480}]") ;;
        a) flags+=(-x -f "${OPTARG:-best}audio/${OPTARG:-best}") ;;
        r) flags+=(--match-title="(?i)${OPTARG}") ;;
        R) flags+=(--match-title="(?i)${OPTARG}") ;;
        d) DIR="$OPTARG" ;;
        h|\?|:|*) display_help ;;
    esac
done
all_params="$@"
shift $((OPTIND-1))
links="$@"

[[ $# -lt 1 ]] && display_help

download "$links"
rerun_if_not_downloaded "$all_params"

# >>93364968
# I don't care. If you wanted to do that with -S all you'd need is -S acodec:opus,br, since it should always have the highest bitrate for video. But normal people care about what actually looks the best.

# >>93364942
# This is about ripping the enhanced bitrate stream which is only available on the VP9 codec, just stfu retard.

# >>93364898
# That's because av1 streams consistently have higher vmaf scores than the vp9 premium bitrate streams.

# >>93364858
# The last time you posted the command you selected av1 in the sorting formats flags like a retard so stfu.

# >>93364629
# Like I said, it's gimped. It requires you to manually select the ID. I've posted the correct command multiple times but you keep calling it bloated despite being fewer total characters.

# >>93364598
# Why is it gimped? point one thing wrong with the command, go ahead.

# >>93364439
# Stop posting this godawful gimped manual command and use the new -S flag like a normal person.

# Command for ripping YT Premium Enhanced Bitrate videos:

# First, check if the video you want has enhanced bitrate available, the format ID you want to see is the "616":

# yt-dlp --list-formats --extractor-args "youtube:player_client=default,ios" yt-link

# Now to rip the video with OPUS audio:

# yt-dlp -f 616+251 --extractor-args "youtube:player_client=default,ios" yt-link
