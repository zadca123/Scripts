#!/usr/bin/env bash
set -euo pipefail # bash strict mode
unset -v latest
for file in ./; do
    [[ $file -nt $latest ]] && latest=$file
done
echo "$latest"
