#!/usr/bin/env bash
set -euo pipefail # bash strict mode

code_exts=".config/Code/User/vscode-extensions.txt"
codium_exts=".config/VSCodium/User/vscodium-extensions.txt"

command -v codium && cat "${DOTFILES}/${codium_exts}" | xargs -n 1 codium --install-extension
command -v code && cat "${DOTFILES}/${code_exts}" | xargs -n 1 code --install-extension
