#!/usr/bin/env bash
set -euo pipefail

DATE="$(date +%F_%H%M%S)"
SCRIPT_NAME="${0##*/}"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${DATE}_${SCRIPT_NAME%%.sh}.log"

display_help() {
    echo "Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>"
    echo "Dependencies: "
    echo "Examples:"
    echo "- ${SCRIPT_NAME} "
    echo "- ${SCRIPT_NAME} "
    exit 2
}
message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ===="
}


mkdir -pv "$LOG_DIR"
ln -rsf "$LOG_DIR" "$HOME"



backup_database(){
    message "Dump of database"
    mysqldump -u root --all-databases > "/var/www/html/sql-backup-${DATE}.sql"
}
backup_everything(){
    message "Archiving everything"
    tar -C /var/www/html/ -cavf "/home/backup/itop-backup-${DATE}.tar.gz" -- .
}


{
    backup_database
    backup_everything
} |& tee -a "$LOG_FILE"
