#!/usr/bin/env bash
set -euo pipefail # bash strict mode

temp="/tmp/launch"
mkdir -p "$temp"

logfilename="${1##*/}_$(date +%F_%H%M%S_%N)"
stdout="${temp}/${logfilename}_stdout.log"
stderr="${temp}/${logfilename}_stderr.log"

echo "== LAUNCH ${*} ==" > "$stdout"
echo "== LAUNCH ${*} ==" > "$stderr"

nohup "$@" >>"$stdout" 2>>"$stderr" &
