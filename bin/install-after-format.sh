#!/usr/bin/env bash
set -euo pipefail # bash strict mode
set -euxo pipefail

setup_flatpak(){
    package-manager.sh install flatpak
    flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install --user -y com.discordapp.Discord
}
setup_shell(){
    package-manager.sh install zsh zsh-autosuggestions zsh-syntax-highlighting
    chsh -s $(which zsh) || sudo usermod -s $(which zsh) "$LOGNAME"
}
setup_music(){
    package-manager.sh install mpd mpd mpc mpdris2 ncmpcpp
    sudo gpasswd -a mpd "$LOGNAME"
    sudo gpasswd -a mpd audio
}
setup_dirs_and_perms(){
    mkdir -p ~/Repos
    sudo mkdir -p /mnt/{crucial_nvme,seagate,toshiba,elements,128gb_sdcard,iso,borg}
    sudo chown "${LOGNAME}:${LOGNAME}" -R /mnt/*
}
setup_printer(){
    package-manager.sh install cups printer-driver-{gutenprint,splix}
    wget -c 'https://ftp.hp.com/pub/softlib/software13/printers/SS/SL-C4010ND/uld_V1.00.39_01.17.tar.gz' -O /tmp/uld.tar.gz
    tar -xavf /tmp/uld.tar.gz
    cd /tmp/uld
    sudo ./install.sh
}
setup_yt(){
    sudo curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp
    sudo chmod a+rx /usr/local/bin/yt-dlp
}
setup_pass(){
    package-manager.sh install pass{,-extension-otp} xdotool ssh-askpass gpg pinentry-{curses,tty,gtk2,gnome3} zbar-tools
    sudo update-alternatives --set pinentry $(which pinentry-gtk-2)
    # passff
    curl -sSL github.com/passff/passff-host/releases/latest/download/install_host_app.sh | bash -s -- firefox
}
setup_gnome(){
    package-manager.sh install gnome-core gnome-tweaks gnome-theme\* \
                       gnome-\*-icon-theme {adapta,materia}-gtk-theme \
                       gnome-shell-extension-appindicator \
                       gnome-shell-extension-volume-mixer
}
setup_qemu(){
    package-manager.sh install qemu \
                       virt-{manager,viewer} spice-{vdagent,client-gtk,webdavd} \
                       dnsmasq vde2 bridge-utils netcat libguestfs0

    # systemctl enable --user --now libvirtd
    sudo usermod -aG libvirt "$LOGNAME"

    # # on guest machine install for clipboard !!!!!!!!!
    # windows: spice-guest-tools-latest, spice-webdav
    # enable rdp in windows

    # how to clipboard and sharing folders
    # https://dausruddin.com/how-to-enable-clipboard-and-folder-sharing-in-qemu-kvm-on-windows-guest/#Step_1_Install_Cockpit_and_Cockpit_Virtual_Machines
}
setup_translator(){
    package-manager.sh install gawk
    git clone https://github.com/soimort/translate-shell /tmp/translate-shell
    cd /tmp/translate-shell/
    make
    sudo make install
}
setup_services(){
    mapfile -t services < <(find "${HOME}/.config/systemd/user/" -type f -iname "*.service")
    sudo systemctl disable --now "${services[@]}" # disable for root
    systemctl enable --now --user "${services[@]}" # enable for user
}
setup_games(){
    protonup -d "${HOME}/.steam/root/compatibilitytools.d/"
    # protonup -y # it crashes
    # sudo apt-get install -y nvidia-driver

    package-manager.sh install libgl1:i386 libc6:i386
    wget -c 'https://cdn.akamai.steamstatic.com/client/installer/steam.deb'
    sudo dpkg -I ./steam.deb
    package-manager.sh update
}
setup_additional_repositories(){
    if command -v apt; then
        sudo apt-add-repository contrib
        sudo apt-add-repository non-free
    elif command -v dnf; then
        sudo dnf install "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"
        sudo dnf install "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
        sudo dnf groupupdate core
    elif command -v zypper; then
        sudo zypper ar -cfp 90 https://ftp.fau.de/packman/suse/openSUSE_Tumbleweed/ packman
        sudo zypper dup --from packman --allow-vendor-change
    fi

}
install_packages(){
    debian_packages=(aria2 atool bc bible-kjv borgbackup bzip2 calibre
                     cmus cockpit cups curl dunst emacs exa fd-find feh ffmpeg
                     flameshot fonts-firacode fonts-hack fonts-inconsolata
                     fonts-monofur fonts-mononoki fonts-terminus fzf fzy
                     git gxkb htop imagemagick inotify-tools jq kitty
                     libnotify-bin lpr lutris moc mpv ncdu neofetch neovim
                     nmon npm ntp oxygencursors pandoc picard picom pigz
                     pixz printer-driver-gutenprint printer-driver-splix
                     pulsemixer python3-pip python3-venv qalc qutebrowser
                     redshift ripgrep rofi rsync scrot shellcheck slock
                     spectrwm subliminal suckless-tools sxhkd sxiv
                     syncthing tcpdump tldr tmux transmission-cli 7zip
                     trash-cli unrar unzip vifm vim vnstat wavemon wget
                     xclip xdotool xsel zathura zathura-cb zathura-djvu
                     zathura-ps zstd)
    command -v tldr && tldr -u &>/dev/null &
    package-manager.sh update
    package-manager.sh install "${debian_packages[@]}"
}
setup_terminal(){
    term="$1"
    sudo update-alternatives --set x-terminal-emulator "$(which "$term")"
}
setup_timezone(){
    sudo timedatectl set-timezone Europe/Warsaw
}

cd /tmp/
setup_additional_repositories
setup_terminal 'kitty'
setup_packages
setup_terminal
! [[ $SHELL =~ zsh ]] && setup_shell
! command -v mpc && setup_music
[[ ! -d ~/Repos ]] && setup_dirs_and_perms
! command -v qemu && setup_qemu
! command -v yt-dlp && setup_yt
! command -v pass && setup_pass
! command -v trans && setup_translator
# setup_services
# setup_printer
# ! command -v flatpak && setup_flatpak
# ! command -v gnome-shell && setup_gnome
