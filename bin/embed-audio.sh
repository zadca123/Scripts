#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# audio=$(find ./ -type f | grep "ogg$\|mp3$\|opus$\|flac$\|wav$\|m4a$" | head -n 1)
audio=$(find-type.sh -a | head -n 1)
while getopts ':a:h' opt; do
    case "$opt" in
        a) audio="$OPTARG" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))
[[ "$#" -lt 1 || ! -f $audio ]] && display_help

for input_file in "$@"; do
    output_file="audio-embedded_${input_file}"
    ffmpeg -i "$input_file" -i "$audio" -c copy -map 0:v:0 -map 1:a:0 "$output_file"
done
