#!/usr/bin/env bash
set -euo pipefail

display_help() {
    echo "Usage: ${0##*/} <file>"
    exit 2
}
[[ $# -lt 1 ]] && display_help


input="$1"
inode="$(stat -c '%i' "${input}")"
partition="$(df --output=target "${input}" | tail -1)"

echo "All files with inode equal ${inode} on ${partition} partition"
find "$partition" -inum "$inode" -ls 2>/dev/null
