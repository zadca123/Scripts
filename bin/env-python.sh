#!/usr/bin/env bash
set -euo pipefail # bash strict mode
venv="${1:-venv}"
[[ ! -d $venv ]] && python3 -m venv "$venv"
source "${venv}/bin/activate"

if [[ -f requirements.txt ]]; then
    pip3 install -r requirements.txt
elif [[ -f pyproject.toml ]]; then
    poetry install
fi
