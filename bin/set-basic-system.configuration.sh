#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} -b -a song.flac"
    echo "Default behaviour: makes webm with audio"
    echo "Where:"
    echo -e "\t -f  --Install flatpak and setup flathub"
    echo -e "\t -d  --Install dwm-distrotube"
    echo -e "\t -s  --Install st-distrotube"
    echo -e "\t -d  --Install st-distrotube"
    echo -e "\t -S  --Install suckless software (dmenu,dwm,st,dwmblocks)"
    echo -e "\t -v  --Setup Vim (Neovim)"
    echo -e "\t -z  --Set zsh default shell"
    echo -e "\t -Z  --Install ZEN kernel (liquorix)"
    echo -e "\t -p  --Download and set up printer drivers (Samsung)"
    echo -e "\t -A  --DO all thing above"
    exit 2
}
most_recent_file=$(find ./ -maxdepth 1 -ctime -1 | tail -n 1)
recentF=$(find ./ -maxdepth 1 -type f -ctime -1 | tail -n 1)
recentD=$(find ./ -maxdepth 1 -type d -ctime -1 | tail -n 1)

# defaults
while getopts ':AfvzZsdph' opt; do
    case "$opt" in
        z)  sudo apt install zsh zsh-completions bash-completions
            chsh -s "$(which zsh)" ;;

        Z)  curl 'https://liquorix.net/add-liquorix-repo.sh' | sudo bash
            sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64 ;;

        s)  apt install build-essential libxft-dev libharfbuzz-dev suckless-tools
            git clone 'https://gitlab.com/dwt1/st-distrotube'
            cd "$most_recent_file" || return 100
            make
            sudo make install ;;

        v)  sudo apt install vim neovim
            curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
                https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim ;;

        d)  git clone 'https://gitlab.com/dwt1/dwm-distrotube'
            cd "$most_recent_file" || return 100
            make
            sudo make install ;;

        p)  # sudo apt install cups printer-drivers-(drivers for cups and samsung)
            wget 'https://ftp.hp.com/pub/softlib/software13/printers/SS/SL-C4010ND/uld_V1.00.39_01.17.tar.gz'
            tar -xvzf "$most_recent_file"
            cd "$most_recent_file" || return 100
            sudo ./install.sh ;;

        f)  sudo apt install flatpak
            flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo ;;

        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))
