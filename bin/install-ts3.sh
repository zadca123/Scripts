#!/usr/bin/env bash
set -euo pipefail # bash strict mode

link="https://files.teamspeak-services.com/releases/client/3.5.6/TeamSpeak3-Client-linux_amd64-3.5.6.run"
wget -c "$link"
chmod +x TeamSpeak3-Client-linux_amd64-3.5.6.run
./TeamSpeak3-Client-linux_amd64-3.5.6.run
mv ./TeamSpeak3-Client-linux_amd64 ~/.ts3client/
