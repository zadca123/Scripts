#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# unset -v latest
# for file in "$dir"/*; do
#   [[ $file -nt $latest ]] && latest=$file
# done

compile_java(){
    file="$1"
    no_ext="${file%.*}"

    # find jar files and add it to the arguments
    for jar in *.jar; do
        [[ -z ${cmd:-} ]] && cmd="-cp ."
        cmd+=":${jar}"
    done

    javac $cmd "$file" && java $cmd "$no_ext"
}


dir=$(dirname "$1")
echo $dir
recent=$(find "$dir" -type f -mtime 0 | head -n1)
ext="${1##*.}"
case "$ext" in
    py) python3 "$1" && black "$1" ;;
    cpp) cd "$dir" && g++ "$1" && ./a.out ;;
    sh) sh "$1" ;;
    lisp) clisp "$1" ;;
    java) compile_java "$@";;
    tex) pdflatex -interaction nonstopmode "$1" ;;
    *) echo "Unrecognized extension: ${ext}" ;;
esac
