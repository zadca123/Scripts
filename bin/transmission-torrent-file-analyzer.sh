#!/usr/bin/env bash
set -euo pipefail # bash strict mode
torrents=$(find "$PWD" -type f -name "*.torrent")

search="^Name: "

for torrent in $torrents; do
    transmission-show "$torrent" | grep "$search" | grep -i --color "${@}"
    [[ $? -eq 0 ]] && echo -e "$torrent\n"
done
