#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [file/s]"
    exit 2
}

[[ $# -lt 1 ]] && display_help

date=$(date '+%Y-%m-%d_%H-%M-%S')
7z a -t7z -m0=lzma2 -mx=9 -aoa -mfb=64 -md=32m -ms=on "archive_${date}.7z" "$@"
