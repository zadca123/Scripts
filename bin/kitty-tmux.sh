#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$'\n\t' # Internal Field Separator, controls word splitting, default is $' \n\t'

term="${1:-kitty}"
lauch_term(){
    $term -- tmux \
          new-session -As0 \; \
          new-window -c "$PWD" -n "${PWD##*/}"
}

# Check if $term and tmux are running
if pgrep "$term" >/dev/null; then
    # Terminal with $term and tmux is already running, bring it into focus
    wmctrl -x -a "${term}.${term}"
else
    # Launch a new terminal with $term and tmux
    launch_term
fi
