case "$1" in
set -euo pipefail # bash strict mode
    create)
        convert "$2" "${@:3}"
        ;;
    *)
        echo Usage: "$0" '[create] [name.gif] [image/s]'
        ;;
esac
