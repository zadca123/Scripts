#!/usr/bin/env bash
set -euo pipefail # bash strict mode
shopt -s extglob
mv +([0-9])x+([0-9])* "$@"
