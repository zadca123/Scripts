#!/usr/bin/env bash
set -euo pipefail # bash strict mode

if command -v locate; then
    ts3="$(locate ts3client_runscript.sh)"
else
    ts3="$(find "${HOME}" -type f -name "ts3client_runscript.sh")"
fi

ts3="${ts3%%$'\n'*}" # only first line
detach.sh "$ts3"
