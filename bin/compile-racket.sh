#!/usr/bin/env bash
set -euo pipefail # bash strict mode

parallel make -C {//} install ::: ${HOME}/Repos/personal/misc-scripts/racket/**/Makefile
