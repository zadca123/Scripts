#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# VIDEOS
# https://www.youtube.com/watch?v=rI-7rgfB5iI&t=182s
# https://www.youtube.com/watch?v=jLRmVNWOrgo


# install_dependencies(){
#     # qemu libvirt bridge-utils virt-manager virt-viewer dnsmasq vde2 netcat-openbsd guestfs-tools
#     sudo apt-get install -y \
#          bridge-utils virt-manager virt-viewer dnsmasq vde2 \
#          netcat-openbsd guestfs-tools \
#          spice-{vdagent,client-gtk,webdavd}
# }

display_help() {
    echo "Usage: ${0##*/} [-iIvch] [-h]"
    echo "Options:"
    echo "  -i <ISO>        : Install a new virtual machine."
    echo "  -I <QEMU_FILE>  : Import an existing VM from the specified file."
    echo "  -v              : View the virtual machine using a graphical interface."
    echo "  -c <VDI_FILE>   : Convert a vdi file to qcow2 format."
    echo "  -h              : Display this help message."
    exit 2
}
[[ $# -lt 1 ]] && display_help

samba_share(){
    qemu-system-x86_64 -net user,smb=/tmp -net nic,model=virtio -name guest="${1:-win2k19_gaming}"
}
define_vars(){
    local ALL_VARS=("$@")

    # Loop through the variables
    for var in "${ALL_VARS[@]}"; do
        IFS='=' read -r var value <<< "$var"
        read -erp "Enter value for $var: " -i "$value" value
        declare -g "$var=$value"
    done
}

while getopts ':i:I:v:c:h' opt; do
    case "$opt" in
        i) define_vars "ISO=${OPTARG}" "NAME=win10" "SIZE=25"
           virt-install \
               --os-variant detect=on \
               --name "$NAME" \
               --memory 2048 \
               --disk "${NAME}.qcow2,size=${SIZE}" \
               --cdrom "$ISO" ;;
        I) define_vars "QEMU_FILE=${OPTARG}" "MEMORY=2048"
           virt-install \
               --import \
               --os-variant detect=on \
               --memory "$MEMORY" \
               --disk "$QEMU_FILE" ;;
        v) virsh start "NAME=win10"
           virt-viewer --connect qemu:///session --domain-name "$NAME" ;;
        c) define_vars "VDI_FILE=${OPTARG}" "QCOW2_FILE=${VDI_FILE%.*}.qcow2"
           qemu-img convert -f vdi -O qcow2 "$VDI_FILE" "QCOW2_FILE" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

virt-manager --connect qemu:///session
