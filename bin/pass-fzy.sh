#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Function to copy the selected field to the clipboard
copy_to_clipboard() {
  local choice="$1"
  local field="$2"
  local value

  # Read the content of the password file
  content=$(pass show "$choice")

  # Determine the value to copy based on the selected field
  if [[ $field == "password" ]]; then
    # Password is always the first line
    value=$(echo "$content" | head -n 1)
  else
    # Find the line that starts with the field and extract its value
    value=$(echo "$content" | grep "^$field:" | cut -d ' ' -f2-)
  fi

  if [[ -n $value ]]; then
    # Copy the value to the clipboard
    echo -n "$value" | xclip -selection clipboard
    echo "${field} copied to clipboard."
  else
    echo "Field '${field}' not found."
  fi
}

pushd "$PASSWORD_STORE_DIR"

# Select the password file using fzy
choice="$(find -L . -type f -name "*.gpg" | fzf)"
choice="${choice%*.gpg}"

# Extract fields from the selected password file
fields=$(pass show "$choice" | grep -o '^[^:]*' | tail -n +2)
fields="password"$'\n'"$fields"

# Select the field to copy using fzy
field=$(echo "$fields" | fzf)

# Copy the selected field to the clipboard
copy_to_clipboard "$choice" "$field"
