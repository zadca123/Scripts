#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [epub,pdf,mobi,etc...] [file/s]"
    exit 2
}

[[ $# -lt 2 ]] && display_help
ext="$1"
shift

for book in "$@"; do
    ebook-convert "$book" "${book%.*}.${ext}" -vv
done
