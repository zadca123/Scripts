#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# adds current playing song in cmus to newly created playlist

[[ ! $(pidof cmus) ]] && echo 'cmus not running...'
[[ $# -ne 1 ]] && echo 'only one argument is possible!!!'

path="/mnt/seagate/multimedia/music"
song=$(cmus-remote -Q | grep "^file" | cut -d' ' -f2-)

playlists="${path}/playlists/${1}"
mkdir -p "$playlists"

cp -vu "$song" "${playlists}/" || echo 'already there'
