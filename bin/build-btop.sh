#!/usr/bin/env bash
set -euo pipefail # bash strict mode
sudo apt-get install -y coreutils sed git build-essential gcc-11 g++-11
git clone https://github.com/aristocratos/btop /tmp/btop
cd /tmp/btop
make
sudo make install
