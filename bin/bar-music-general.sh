#!/usr/bin/env bash
set -euo pipefail # bash strict mode
if [[ $(pidof cmus) ]]; then
    statuss=$(cmus-remote -Q | awk '$1=="status" {$1="";print substr($0,2)}')
    album=$(cmus-remote -Q | awk '$2=="album" {$1=$2="";print substr($0,3)}')
    artist=$(cmus-remote -Q | awk '$2=="artist" {$1=$2="";print substr($0,3)}')
    title=$(cmus-remote -Q | awk '$2=="title" {$1=$2="";print substr($0,3)}')
    # composer=$(cmus-remote -Q | awk '$2=="composer" {$1=$2="";print substr($0,3)}')
    vol=$(cmus-remote -Q | awk '$2=="vol_left" {$1=$2="";print substr($0,3)}')
    most=$(cmus-remote -Q \
            | grep "tag album\|tag albumartist\|tag composer\|tag artist\|tag title" \
        | cut -d' ' -f3-)

    no_tags=$(cmus-remote -Q | awk '$1=="file" {$1=$2="";print substr($0,3)}')

    if [[ $title == '' ]]; then
        # echo -e "${statuss} > ${no_tags} > 🔊${vol}%"
        echo "${no_tags:0:120} > ${vol}%"

    else
        # echo -e "${statuss} > ${title} > ${album} > ${artist} > 🔊${vol}%"
        echo -e "${statuss} > ${title:0:80} > ${album:0:50} > ${artist:0:50} > ${vol}%"
    fi
elif [[ $(pidof mocp) ]]; then
    mocp --format '%state > %song > %album > %artist'
elif [[ $(pidof mpd) ]]; then
    mpc --format '%title% > %album% > %artist%' | head -n1
fi
