#!/usr/bin/env bash -x
set -euo pipefail # bash strict mode
# #!/usr/bin/env bash
set -euo pipefail # bash strict mode
basename=${1##*/}
noext=${basename%.*}
directory="/tmp/your/path/${noext}"
mkdir -p "$directory"

length="$(ffprobe -i "$1" -show_entries format=duration -v quiet -of csv='p=0')"
image_count=$( echo "l(${length})/l(1.2)" | bc -l)
framerate=$(ffprobe -v 0 -of csv="p=0" -select_streams V:0 -show_entries stream=r_frame_rate "$1")
step=$( echo "${length}/${image_count}" | bc -l)
round(){
    echo "(${1}+0.5)/1" | bc
}

# for (( i=0; i<$(round $length) ; i+=$(round $step) )); do
# for (( i=0; i<$(round $length) ; i+=$(round $image_count) )); do
#     timestamp=$(date -u -d "@${i}" "+%H:%M:%S")
#     ffmpeg -y -i "$1" -ss "$timestamp" -vframes 1 -vcodec png -an -y "${directory}/${timestamp}.png"
#     mogrify -fill red -pointsize 100 \
    #         -draw "text 50,50 '${timestamp}'" \
    #         "${directory}/${timestamp}.png"
#     done

# montage "${directory}/*" -geometry +10+10 grid.png

# https://askubuntu.com/questions/874042/how-to-make-screenshot-gallery-from-command-line
rows=$(round "sqrt(${image_count})")
cols=$((rows + 1))
frames=$(round "(${framerate})*${image_count}")
ffmpeg -i "$1" -frames 1 \
    -vf "select=not(mod(n\,${frames})),scale=-1:240,tile=${rows}x${cols}" \
    grid.png

# ffmpeg -i "$1" -frames 1 -vf "select=not(mod(n\,1000)),scale=-1:240,tile=2x3" out.png

# I'd use "ffmpeg" to do this. A command to extract PNG images at X intervals can be as simple as this...
# ffmpeg -i input.mp4 -r 1 out%04d.png
# Using the option -r 1 will set the output frame rate to 1 every second. Assuming the input video has a frame rate of 25 per second, the option -r 1 will output about every 25th frame. Calculate the interval you need according to the frame rate of the input. For example, to get approximately every 10th frame, use -r 2.5.
# The output file name in this example contains %04d, which creates a numbered sequence with four digits, padded with leading zeros.
