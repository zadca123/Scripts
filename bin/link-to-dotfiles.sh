#!/usr/bin/env bash
set -euo pipefail # bash strict mode

dotfiles_dir="${DOTFILES:-${HOME}/.config/dots}"

for file in "$@"; do
    [[ -L "$file" ]] && echo "${file} is a symlink already" && continue

    file="$(realpath "$file")"
    file_wo_home="${file##${HOME}/}"
    file_dirname="$(dirname "$file")"
    file_dirname_wo_home="${file_dirname##${HOME}}"

    # # Remove leading slash from file_dirname_wo_home if present
    # file_dirname_wo_home="${file_dirname_wo_home#/}"

    # for files and directories
    mkdir -pv "${dotfiles_dir}/${file_dirname_wo_home}"
    mv -v "$file" "${dotfiles_dir}/${file_dirname_wo_home}"
    ln -sv "${dotfiles_dir}/${file_wo_home}" "$file_dirname"

    # dry run
    # echo "You want to create directory: ${dotfiles_dir}/${file_dirname_wo_home}"
    # echo "You want to move: ${file} >> ${dotfiles_dir}/${file_dirname_wo_home}"
    # echo "You want to link: ${dotfiles_dir}/${file_wo_home} >> ${file_dirname}"
done
