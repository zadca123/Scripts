#!/usr/bin/env bash
set -euo pipefail # bash strict mode
msgId="291049"
if [[ ! $(pidof cmus) ]]; then
    message="cmus is not currently running..."
    echo "$message"
    # dunstify -r "$msgId" "$message"
    exit 2
fi

cover-music-generate.sh "$(cmus-remote -Q | awk '$1=="file" {$1="";print substr($0,2)}')" "${1:-200}"

most=$(cmus-remote -Q \
        | sed 's/^tag //g' \
        | grep "^status\|^album\|^albumartist\|^composer\|^artist\|^title" \
        | cut -d' ' -f1- \
        | sed 's/ /\t/' \
    | column -t -s $'\t')
# >>but also, just in case the tag strings contain tabs, they should be replaced by sed before using the above. "␉" is apparently a unicode symbol meant to represent horizontal tab, but spaces or question marks or whatever would obviously work

# most=$(cmus-remote -Q | awk '$1~/status/||$2~/album|albumartist|composer|artist|title/{gsub(/^tag/,"");printf("%-10s",$1);$1="";print}')
tags_not_present=$(cmus-remote -Q | grep ^file | tr -s '/' '\n' | tail -n3)

position=$(date -d@"$(cmus-remote -Q | awk '$1=="position" {print $2}')" -u +%H:%M:%S)
duration=$(date -d@"$(cmus-remote -Q | awk '$1=="duration" {print $2}')" -u +%H:%M:%S)
vol=$(cmus-remote -Q | awk '$2=="vol_left" {$1=$2="";print substr($0,3)}')

tags="${most}"$'\n'"${position}/${duration}"$'\n'"Volume: ${vol}%"
no_tags="${tags_not_present%.*}"$'\n'"${position}/${duration}"$'\n'"Volume: ${vol}%"
title="cmus-notifier"
if [[ $most == "" ]];then
    notify-send -i "/tmp/cover.png" "$title" "$no_tags"
    # dunstify -i "/tmp/cover.png" -r "$msgId" "$title" "$no_tags"
else
    notify-send -i "/tmp/cover.png" "$title" "$tags"
    # dunstify -i "/tmp/cover.png" -r "$msgId" "$title" "$tags"
fi

# set this in cmus
# :set status_display_program=<path-to-the-shell-script>
# or set if from console
# cmus-remote -C set status_display_program=<path-to-the-shell-script>
