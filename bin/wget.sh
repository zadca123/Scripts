#!/usr/bin/env bash
set -euo pipefail # bash strict mode

wget \
    --mirror \
    --warc-file=YOUR_FILENAME \
    --warc-cdx \
    --page-requisites \
    --html-extension \
    --convert-links \
    --execute robots=off \
    --directory-prefix=. \
    --span-hosts \
    --domains=example.com,www.example.com,cdn.example.com \
    --wait=10 \
    --random-wait \
    "$1"

# --user-agent=Mozilla (mailto:archiver@petekeen.net) \
