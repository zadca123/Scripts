#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# lauch these programs at autostart only once
# add programs to this array

programs=(dunst mpd redshift transmission-daemon sxhkd gxkb
          wallpaper-set-random.sh 'emacs --daemon')

notify(){
    message="${1}: ${@:2}"
    notify-send "$message"
    echo "$message"
}

check_status(){
    base="${1%% *}" # command without flags

    if ! command -v "$base" 1>/dev/null; then
        notify "$base" "not installed"
        return 2 # for continue
    elif ! pgrep -f "$base" 1>/dev/null; then
        notify "$base" "starting"
        return 2 # for continue
    fi

    notify "$base" "already running"
}

for prog in "${programs[@]}"; do
    check_status "$prog" && continue
    $prog &>/dev/null &
done
