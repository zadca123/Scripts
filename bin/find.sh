#!/usr/bin/env bash
set -euo pipefail # bash strict mode
BOLD='\e[1;31m'         # Bold Red
REV='\e[1;32m'       # Bold Green
display_help(){
    echo -e "${BOLD}Usage:  [starting-location] [name-of-searching-file] ${OFF}"
    echo -e "${BOLD}Usage:${REV} $(basename $0) -e ~/Torrents/transmission_finished ${OFF}"
    echo -e "${REV}The following switches are recognized. ${OFF} "
    echo -e "${REV}-e ${BOLD}  --Searches and deletes empty files and directories in current of specified path ${OFF}"
    echo -e "${REV}-p ${BOLD}  --Searches for *.part files (unfinished transmission torrent) and deletes them (use with causion) ${OFF}"
    echo -e "${REV}-s +-=[size (in megabytes)] ${BOLD}  --Searches for files larger than specified size${OFF}"
    echo -e "${REV}-t +-=[days] ${BOLD}  --Searches for files modified from now to specified days back${OFF}"
    echo -e "${REV}-h${BOLD}  --Displays this help message. No further functions are performed.${OFF}"
    exit 2
}

[[ $# -lt 1 ]] && display_help
if [ -d "$1" ]; then
    PWD="$1"
    shift
else
    PWD=$(pwd -P)
fi

fd="find $PWD -mount"
# In case you wanted to check what variables were passed
# echo "flags = $*"
while getopts ':n:s:t:eph' opt; do
    case $opt in
        n) $fd -iname "*${OPTARG}*" | grep --color -i "$OPTARG" ;;
        e) list=$($fd -empty) ;;
            #e) $fd -empty -exec trash-put "{}" \; -print ;;
        p) list=$($fd -type f -name "*.part$") ;;
            # p) $fd -type f -name "*.part$" -exec trash-put "{}" \; -print ;;
        s) $fd -type f -size "${OPTARG}M" -exec du -h "{}" \; | sort -n ;;
            # t) $fd -type f -mtime "-${OPTARG}" -exec sort -c '%y %n' "{}" \; | sort -n ;;
        t) $fd -type f -mtime "${OPTARG}" -exec ls -gort --full-time "{}" \; | sed 's/\..........//' | cut -d' ' -f4,5,7- ;;
        h|\?|:|*) display_help ;;
    esac
done

case "$1" in
    -e | -p)
        echo "$list"
        read -rp 'Do you want to delete above files? (y,n): ' ans
        [[ $ans == "Y" || $ans == 'y' ]] && trash-put "$list"
        # (trash-put -v $list | grep -v "Volume of file: \|Trash-dir: ") # something dont work woth this grep
        ;;
    *)exit 2 ;;
esac
