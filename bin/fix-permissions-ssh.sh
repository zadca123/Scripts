#!/usr/bin/env bash
set -euo pipefail # bash strict mode

chmod go-w "${HOME}"
chmod 700 "${HOME}/.ssh"
chmod 600 "${HOME}/.ssh/authorized_keys"

sudo systemctl restart sshd
