#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} [string]"
    exit 2
}
[[ $# -lt 1 ]] && display_help

input=$1

function generate_combinations {
  if [ ${#1} -eq 0 ]; then
    echo "$2"
  else
    char=${1:0:1}
    rest=${1:1}
    generate_combinations "$rest" "$2$char"
    generate_combinations "$rest" "$2${char^^}"
    if [[ "$char" =~ [[:alpha:]] ]] && [[ "${char,,}" != "${char^^}" ]]; then
      generate_combinations "$rest" "$2${char,}${rest^^}"
      generate_combinations "$rest" "$2${char^^}${rest,,}"
    fi
  fi
}

generate_combinations "$input" ""
