#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} [pattern-of-torrent-name]"
    exit 2
}
[[ $# -lt 1 ]] && display_help

torrent_ids=$(transmission-remote -l | grep -i "$1")
echo "$torrent_ids"
[[ -z $torrent_ids ]] && exit

read -rp "Type torrent number: " torrent_id
transmission-remote -t "$torrent_id" -l

read -rp "Type path to search for files of the torrent: " path
transmission-remote -t "$torrent_id" --find "$path"
