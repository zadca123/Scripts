##!/usr/bin/env bash
set -euo pipefail

DATE="$(date +%F_%H%M%S)"
SCRIPT_NAME="${0##*/}"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME%%.sh}_${DATE}.log"

display_help() {
    error_message="$*"
    [[ -n ${error_message:-} ]] && echo -e "Error: ${error_message}\n"

    cat <<EOF
Description: backup files or directories
Usage: ${SCRIPT_NAME} [optional arg/s] <mandatory arg/s>
Dependencies: tar, gunzip
Examples:
- ${SCRIPT_NAME} <directory or file>
EOF
    exit 2
}
message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ====" | tee -a "$LOG_FILE"
}


[[ $# -ne 1 ]] && display_help
mkdir -pv "$LOG_DIR"
ln -rsf "$LOG_DIR" "$HOME"

input_file="$1"
ARC="${input_file}.tar.gz"
TAR_OPTS=(-cavf "$ARC")

if [[ -d $input_file ]]; then
    TAR_OPTS+=(-C "$input_file" .)
else
    TAR_OPTS+=(-C $(dirname $input_file) $(basename $input_file))
fi


tar "${TAR_OPTS[@]}"
