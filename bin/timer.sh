#!/usr/bin/env bash
set -euo pipefail # bash strict mode
time=1
while :; do
    clear
    echo $time
    ((time+=1))
    sleep 1
done
