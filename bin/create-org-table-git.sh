#!/usr/bin/env bash
set -euo pipefail # bash strict mode

echo "| Date | Hash | Message | File |"
echo "|------------------------------|"
GIT_PAGER="" git ls-tree -r --name-only HEAD | while read filename; do master
    last_commit=$(git log -1 --format="%ai | %H | %s" -- "$filename")
    echo "| $last_commit | $filename |"
done
