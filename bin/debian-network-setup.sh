#!/usr/bin/env bash
set -euo pipefail # bash strict mode
read -rp "type wifi name (ssid): " wifi_name
read -rp "type wifi password (psk): " wifi_password
read -rp "type interface name to listen (try: ip a): " interface_name

echo "ctrl_interface=/run/wpa_supplicant
      update_config=1
      network={
             ssid=${wifi_name}
             psk=${wifi_password}
}" > /etc/wpa_supplicant/wpa_supplicant.conf

echo "allow-hotplug ${interface_name}
      iface ${interface_name} inet dhcp
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" > /etc/network/interfaces

wpa_supplicant -B -i "${interface_name}" -c /etc/wpa_supplicant/wpa_supplicant.conf

systemctl enable wpa_supplicant.service
systemctl restart wpa_supplicant.service

# >>86119175
# nano /etc/wpa_supplicant/wpa_supplicant.conf

#      ctrl_interface=/run/wpa_supplicant
#     update_config=1
#     network={
#              ssid="your_wifi_name"
#              psk="your_wifi_password"
#     }

# nano /etc/network/interfaces

#     allow-hotplug wlp3s0
#     iface wlp3s0 inet dhcp
#     wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

# wpa_supplicant -B -i wlp3s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
# dhclient wlp3s0

# >>86119436
# or
# systemctl enable wpa_supplicant.service
# systemctl enable dhcpcd.service
# systemctl restart wpa_supplicant.service
# systemctl restart dhcpcd.service
# at the end, I keep forgetting.

# >>86459594 (You)
# >/etc/network/interfaces
# Are you really using sysV init?
# >sourcing /etc/network/interfaces
# This can't be right.
# >ipv6 not works
# Are you sure you have IPv6 connectivity to begin with or are you just missing the link-local?
# Anyway, to enable a disabled IPv6:

# sysctl net.ipv6.conf.wlx08beac1a726c.disable_ipv6=0
# sysctl net.ipv6.conf.wlx08beac1a726c.autoconf=1
# sysctl net.ipv6.conf.wlx08beac1a726c.accept_ra=1
