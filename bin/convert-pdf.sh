#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# dependency: poppler-utils
display_help(){
    echo "Usage: ${0##*/} [operation] [file(s)]"
    echo "Operations:"
    echo -e "\t img, image\tConvert PDF to image"
    echo -e "\t txt, text\tConvert PDF to text file"
    echo -e "\t compress\tCompress PDF"
    echo -e "\t merge\t\tMerge multiple PDFs"
    exit 2
}

[[ $# -lt 2 ]] && display_help

pdf2text(){
    pdftotext -nopgbrk "$1"
}
pdf2img(){
    pdftoppm "-${1}" "$1" "${1%.pdf}"
}

pdf_compress(){
    output="${1%.pdf}_compressed.pdf"
    # images are awful with /screen
    # gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH "-sOutputFile=${output}" "$1"
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH "-sOutputFile=${output}" "$1"
}

pdfs_merge(){
    output="${1//.pdf/}.pdf"
    gs -dNOPAUSE -sDEVICE=pdfwrite "-sOUTPUTFILE=${output// /-}" -dBATCH "${@:2}"

}

opt="$1"
files="${@:2}"
case "${opt,,}" in
    img|image) pdf2img "$files";;
    txt|text) pdf2text "$files";;
    compress) pdf_compress "$files";;
    merge) pdfs_merge "$files";;
    h|\?|:|*) display_help ;;
esac
