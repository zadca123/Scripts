#!/usr/bin/env bash
set -euo pipefail # bash strict mode

DATE="$(date +%F_%H%M%S)"
SCRIPT_NAME="${0##*/}"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME%%.sh}_${DATE}.log"
SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
EXCLUDE_FILE="${SCRIPT_DIR}/exclude-file-rsync.txt"
ARC="/tmp/${SCRIPT_NAME}_$(date +%F).tar.xz"
RSYNC_OPTS=(-rzvhPC --mkpath --compress-choice=lz4
            --log-file="$LOG_FILE"
            --exclude-from="$EXCLUDE_FILE")
SOURCES=()

display_help() {
    cat <<EOF
Usage: ${SCRIPT_NAME} [options] -S <source directory/s> -D <destination directory>
Dependencies: inotify-tools rsync (optional: entr)
Options:
  -d, --delete           Delete extraneous files from destination dirs
  -e, --exclude PATTERN  Exclude files matching PATTERN
  -a, --archive          Archive source before syncing
  -D, --destination DIR  Specify the destination directory
  -S, --source DIR       Specify the source directory/ies
  -h, --help             Show this help message
Examples:
  ${SCRIPT_NAME} -d -e '*.tmp' -a -S dir1 -D user@111.111.111.111:~/somedir
  ${SCRIPT_NAME} -S dir1 -S dir2 -D /tmp/dir3
EOF
    exit 2
}

message() {
    local info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ===="
}

while getopts ":de:e:D:S:ah" opt; do
    case ${opt} in
        d ) RSYNC_OPTS+=(--delete --delete-after) ;;
        e ) EXCLUDES+=(--exclude="${OPTARG}") ;;
        a ) DO_COMPRESS="true" ;;
        D ) DESTINATION="$OPTARG" ;;
        S ) SOURCES+=("$OPTARG") ;;
        h ) display_help ;;
        \? ) echo "Invalid option: -$OPTARG" >&2; display_help ;;
        : ) echo "Invalid option: -$OPTARG requires an argument" >&2; display_help ;;
    esac
done
shift $((OPTIND -1))

[[ -z ${SOURCES:-} || -z ${DESTINATION:-} ]] && display_help
mkdir -pv "$LOG_DIR"
ln -rsf "$LOG_DIR" "$HOME"

# Set SOURCES and DESTINATION based on remaining arguments
DESTINATION_NO_PATH="${DESTINATION//:*/}"
DESTINATION_ARC="${DESTINATION_NO_PATH}:${ARC}"
RSYNC_OPTS+=("${EXCLUDES[@]}")


cmd() {
    rsync "${RSYNC_OPTS[@]}" "$@"
}

archive() {
    message "Creating ${ARC}"
    tar --exclude-vcs --exclude-vcs-ignores -C "$(dirname "${SOURCES[1]}")" -caf "$ARC" "$(basename "${SOURCES[1]}")"
}

unarchive_on_destination() {
    message "Unarchiving ${ARC} on ${DESTINATION_NO_PATH}"
    ssh "$DESTINATION_NO_PATH" -t "tar -xvf ${ARC}"
}

first_sync() {
    message "First sync"
    if [[ ! -f $ARC ]]; then
        archive
        cmd -- "$ARC" "${DESTINATION_ARC}"
        unarchive_on_destination
    fi
}

sync_dirs() {
    cmd -- "${SOURCES[@]}" "$DESTINATION"
}

entr_sync() {
    # entr goes nuts when file gets deleted
    find "${SOURCES[1]}" -type f | entr "$(sync_dirs)"
}

dynamic_sync() {
    message "Dynamic sync"
    sync_dirs
    while inotifywait -r -e modify,create,move "${SOURCES[@]}"; do
        sync_dirs
    done
}


[[ ${DO_COMPRESS:-} ]] && first_sync
dynamic_sync
