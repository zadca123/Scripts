#!/usr/bin/env bash
set -euo pipefail # bash strict mode
{
    # profile=$(awk -v RS="(^|\n)\\\[[a-zA-Z0-9]*\\\](\n|$)" '/\nDefault=1/' "$HOME/.mozilla/firefox/profiles.ini" | sed -n 's/^Path=\(.*\)$/\1/p')
    # if [ -z "$profile" ]; then
    #     echo "couldn't find profile"
    #     exit 1
    # fi

    # profile_dir="$HOME/.mozilla/firefox/$profile"
    # if [ ! -e "$profile_dir" ]; then
    #     echo "Failed to locate profile"
    #     exit 1
    # fi

    TMPDB=$(mktemp -u)
    # cp "$profile_dir/cookies.sqlite" "$TMPDB"
    cookies="$1"
    cp "$cookies" "$TMPDB"
    {
        echo "# Netscape HTTP Cookie File"
        sqlite3 -separator "$(printf '\t')" "$TMPDB" <<EOF
.mode tabs
.header off
select host,
case substr(host,1,1)='.' when 0 then 'FALSE' else 'TRUE' end,
path,
case isSecure when 0 then 'FALSE' else 'TRUE' end,
expiry,
name,
value
from moz_cookies;
EOF
    }
    rm -f "$TMPDB"
} >~/cookies.txt
