#!/usr/bin/env bash
set -euo pipefail # bash strict mode
SCRIPT_NAME="${0##*/}"

display_help() {
    echo "Usage: ${SCRIPT_NAME} <other commands>"
    echo "Dependencies: inotify-tools"
    echo "Examples:"
    echo "- ${SCRIPT_NAME} rsync -ruhP ./dir sever:/path/idk"
    exit 2
}
message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ===="
}


for dir in "$@"; do
    [[ -d $dir ]] && DIR="$dir"
done

[[ $# -lt 2 && ! -d ${DIR:-} ]] && display_help

while inotifywait -r -e modify,create,move "$DIR"; do
    message "Running: ${*}"
    "$@"
done
