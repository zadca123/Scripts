#!/usr/bin/env bash
set -euo pipefail # bash strict mode
#TODO: check this https://askubuntu.com/questions/194427/what-is-the-terminal-command-to-take-a-screenshot
display_help (){
    echo "Usage: ${0##*/} [m|u|s|c|1|4] {optional: 'upload'}"
    echo "Where first agruments must be:"
    echo -e "\t fullscreen | m --Takes screenshot of full screen"
    echo -e "\t focused | u \t--Takes screenshot of currently focused window"
    echo -e "\t selection | s \t--Lets you draw reclangle, saves screenshot"
    echo -e "\t clipboard | c \t--Lets you draw reclangle, takes screenshot to clipboard"
    echo -e "\t 16x9 | 1 \t--Takes screenshot of center, 16x9"
    echo -e "\t 4x3 | 4 \t--Takes screenshot of center, 4x3"
    echo ""
    echo -e "If your second argument will be 'upload', then screenshot will be uploaded and link saved to clipboard"
    echo -e "(it will overwrite screenshot in clipboard from -c flag if used)"
    exit 2
}

# # I have set this variable global, so I dont have to set it here
# SCREENSHOTS="${HOME}/Documents/screenshots/"
tmp="${SCREENSHOTS}/clipboard.png"
name='%Y-%m-%d-%H%M%S_$wx$h.png'

scrot_select="scrot -s -f -l style=dash,width=3,color=cyan"
case $1 in
    fullscreen | full | m) scrot -m "$name" -e 'mv $f $$SCREENSHOTS' ;;
    focused | focus | u) scrot -u "$name" -e 'mv $f $$SCREENSHOTS' ;;
    16x9 | 1) scrot -m -a 320,0,1920,1080 "$name" -e 'mv $f $$SCREENSHOTS' ;;
    4x3 | 4) scrot -m -a 560,0,1440,1080 "$name" -e 'mv $f $$SCREENSHOTS' ;;
    selection | s) sleep 1; $scrot_select "$name" -e 'mv $f $$SCREENSHOTS' ;;
    clipboard | clip | c) sleep 1; $scrot_select -o "$tmp"; xclip -sel c -t image/png "$tmp" ;;
    *) display_help ;;
esac

# most recent file in screenshot directory
recent="${SCREENSHOTS}/$(ls -At "$SCREENSHOTS" | head -n1)"

notify-send -i "$recent" "$1 screenshot taken"

[[ $2 == 'upload' ]] && upload.sh "$recent"

# 4chan
# Could be something to do with the filenames being the same and overwriting each other. Try this command instead, it makes the filenames different from one another
# scrot '/tmp/%F_%T_$wx$h.png' -e 'xclip -selection clipboard -target image/png -i $f'
# and for using the mouse to draw a rectangle to screenshot, just add the -s argument
# scrot -s '/tmp/%F_%T_$wx$h.png' -e 'xclip -selection clipboard -ta
