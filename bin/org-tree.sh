#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# Define the function to create org-mode directory tree
function create_org_tree() {
    local DIR="$1"
    local INDENT="$2"

    echo "$INDENT* $(basename "$DIR")"

    for file in "$DIR"/*; do
        if [[ -d "$file" ]]; then
            create_org_tree "$file" "$INDENT""  "
        elif [[ -f "$file" ]]; then
            echo "$INDENT  - $(basename "$file")"
        fi
    done
}

# Get the directory to create the org-mode directory tree for
read -p "Enter the directory path to create org-mode directory tree for: " DIR_PATH

# Check if the directory exists
if [[ ! -d "$DIR_PATH" ]]; then
    echo "Error: Directory does not exist."
    exit 1
fi

# Create the org-mode directory tree
create_org_tree "$DIR_PATH" ""
