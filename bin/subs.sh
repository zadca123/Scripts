#!/usr/bin/env bash
set -euo pipefail # bash strict mode

DIR="subs"
mkdir -p "$DIR"


languages=(en ru de pl)
cmd(){
    local video="$1"
    for lang in "${languages[@]}"; do
        subliminal --debug --opensubtitles "zadca123" "Zadca123@" download -l "$lang" -d "$DIR" "$video"
    done
}
for video in "$@"; do
    cmd "$video" &
done

wait
