#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# TODO: implement filters on images with imagemagick
display_help(){
    echo "Usage: ${0##*/} [path-to-search-for-wallpapers] [center|fill|max|scale|tile]"
    exit 2
}
[[ $# -lt 1 || ! -d $1 ]] && display_help

ROOT_PATH="$(realpath "$1")"
wallpaper_paths_file="${HOME}/.wallpapers-paths_${ROOT_PATH//\//_}.txt"
feh_wallpaper_flag="--bg-${2:-max}"

fill_wallpapers_file(){
    find \
        -L "$ROOT_PATH" \
        -mount \
        -type f \
        -size +500k \
        -size -10M \
        -iregex ".*\.\(png\|jpe?g\)$" \
        -not -iname "*screenshot*" \
        2>/dev/null | shuf > "$wallpaper_paths_file"
}
check_if_wallpaper_correct(){
    wallpaper="$1"
    echo "Setting ${wallpaper}"
    if [[ -z $wallpaper ]]; then
        echo "ERROR: File is empty? ${wallpaper}"
        exit 2
    fi
}
set_wallpaper(){
    wallpaper="$1"
    check_if_wallpaper_correct "$wallpaper"
    if pgrep gnome-shell; then
        gsettings set org.gnome.desktop.background picture-uri "file:///${wallpaper}"
        # other options: none, wallpaper, centered, scaled, stretched
        gsettings set org.gnome.desktop.background picture-options scaled

        # color=$(convert "$wallpaper" -scale 1x1\! -format '%[pixel:u]' info:-)
        # gsettings set org.gnome.desktop.background primary-color "$color"
    elif pgrep mate; then
        gsettings set org.mate.background picture-filename "$wallpaper"
    else
        feh "$wallpaper" "$feh_wallpaper_flag"
    fi
}
get_next_wallpaper(){
    head -n1 "$wallpaper_paths_file"
    sed -i '1d' "$wallpaper_paths_file" # delete first wallpaper path
}
get_remaining_wallpaper_info(){
    echo "Remaining wallpapers: $(wc -l $wallpaper_paths_file)"
}

if [[ ! -f $wallpaper_paths_file || $(awk 'END {print NR}' "$wallpaper_paths_file") -le 1 ]]; then
    fill_wallpapers_file
fi

set_wallpaper "$(get_next_wallpaper)"
get_remaining_wallpaper_info
