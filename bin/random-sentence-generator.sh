#!/usr/bin/env bash
set -euo pipefail # bash strict mode
cat /usr/share/dict/words \
    | shuf -n "${1:-5}" \
    | tr '\n' ' '
