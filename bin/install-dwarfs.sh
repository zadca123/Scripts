#!/usr/bin/env bash
set -euo pipefail # bash strict mode
cd /tmp

sudo apt install -y \
    g++ \
    clang \
    cmake \
    make \
    bison \
    flex \
    ronn \
    fuse3 \
    pkg-config \
    binutils-dev \
    libarchive-dev \
    libboost-context-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-python-dev \
    libboost-regex-dev \
    libboost-system-dev \
    libboost-thread-dev \
    libevent-dev \
    libjemalloc-dev \
    libdouble-conversion-dev \
    libiberty-dev \
    liblz4-dev \
    liblzma-dev \
    libssl-dev \
    libunwind-dev \
    libdwarf-dev \
    libelf-dev \
    libfmt-dev \
    libfuse3-dev \
    libgoogle-glog-dev

git clone --recurse-submodules https://github.com/mhx/dwarfs
cd dwarfs

mkdir build
cd build
cmake .. -DWITH_TESTS=1
make -j$(nproc)
