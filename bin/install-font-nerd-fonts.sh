#!/usr/bin/env bash
set -euo pipefail # bash strict mode

if [[ $EUID -ne 0 ]]; then
    echo run as sudo!
    echo "sudo ${0}"
    exit 2
fi

# latest=$(curl -I https://github.com/ryanoasis/nerd-fonts/releases/latest | grep 'location: ' | tr -d '\r')
# latest_tag="${latest##*/}"
font_names=(Gohu NerdFontsSymbolsOnly)

cd /usr/share/fonts
for font in "${font_names[@]}"; do
    archive="${font}.zip"
    link="https://github.com/ryanoasis/nerd-fonts/releases/latest/download/${archive}"
    wget -c "$link"
    unzip "$archive" -d "$font"
done
rm -rf ./*.zip
fc-cache -vf
