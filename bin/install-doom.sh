#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} -g git-2.8.5 -e emacs-27.1"
    echo "Default behaviour: helps to install doom emacs and dependencies on debian based ditros"
    echo "Where:"
    echo -e "\t -f  --Fix DOOM Emacs if breaks, needed latest emacs and git version"
    echo -e "\t -g  --Set git version (default: ${gitt})"
    echo -e "\t -e  --Set emacs version (default: ${emacss})"
    echo -e "\t -G  --List all avaiable git versions"
    echo -e "\t -E  --List all avaiable emacs versions"
    # exit 2
}
display_help
# defaults
gitt='git-2.9.5'
emacss='emacs-27.2'
while getopts ':fg:Ge:Eh' opt; do
    case "$opt" in
        g) gitt=${OPTARG} ;;
        G) curl https://mirrors.edge.kernel.org/pub/software/scm/git/ | grep "git-[0-9]" | grep -v "gz\|sign" | cut -d'"' -f2
            exit 0 ;;
        e) emacss=${OPTARG} ;;
        E) curl https://mirrors.edge.kernel.org/pub/software/scm/git/ | grep "emacs-[0-9]" | grep -v "gz\|sign" | cut -d'"' -f2
            exit 0 ;;
        f) cd ~/.emacs.d || return
            rm -rf .local
            git pull
            bin/doom install
            exit 0 ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))
[[ $# -gt 0 ]] && display_help && exit 2

if [[ -f /bin/pacman ]]; then
    sudo pacman -S git emacs
elif [[ -f /bin/apt ]]; then
    sudo apt update
    # git and emacs dependencies, also atool to unpack archive
    sudo apt install dh-autoreconf libcurl4-gnutls-dev libexpat1-dev \
        gettext libz-dev libssl-dev asciidoc xmlto docbook2x install-info \
        libc6-dev libjpeg62-dev libncurses5-dev libpng-dev libtiff4-dev \
        libungif4-dev xaw3dg-dev zlib1g-dev libice-dev libsm-dev libx11-dev \
        libxext-dev libxi-dev libxmu-dev libxmuu-dev libxpm-dev libxrandr-dev \
        libxt-dev libxtrap-dev libxtst-dev libxv-dev x-dev xlibs-static-dev \
        atool

    wget -c "https://mirrors.edge.kernel.org/pub/software/scm/git/${gitt}.tar.xz"
    # tar -xvf "${gitt}.tar.xz"
    aunpack "${gitt}.tar.xz"
    cd "$gitt" || return
    make configure
    ./configure --prefix=/usr
    make all doc info
    sudo make install install-doc install-html install-info
    cd || return

    wget -c "https://mirrors.kernel.org/gnu/emacs/${emacss}.tar.xz"
    # tar -xvf "${emacss}.tar.xz"
    aunpack "${emacss}.tar.xz"
    cd "$emacss" || return
    ./configure
    make
    make install
    cd || return
else
    echo display_help
fi

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install
