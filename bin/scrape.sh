#!/usr/bin/env bash

ROOT="${HOME}/Documents/websides" # without slash at end!!!!
download(){
    link="$1"
    subdir=$(echo "$link" | sed 's~^https\?://~~')
    DEST="${ROOT}/${subdir}"
    wget -r -l inf -P "$DEST" "$link"
}


ulimit -f 25600 # don't allow to download too big file

for link in "$@"; do
    download "$link"
done

ulimit -f 99999999999999
