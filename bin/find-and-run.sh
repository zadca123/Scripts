#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [patterns]"
    exit 2
}

error() {
    echo "$0: $*" >&2
    exit 1
}

[[ $# -lt 1 ]] && display_help

# FZF_DEFAULT_COMMAND="fdfind --ignore-file=${HOME}/.fdignore"
# FZF_DEFAULT_OPTS="--layout=reverse --height 40%"

actions=(vim nvim cd emacs 'emacsclient -a "" -c')
file_or_dir="$(locate -i "${*}" | fzy)"
action=$(printf "%s\n" "${actions[@]}" | fzy)


$action "$file_or_dir"
