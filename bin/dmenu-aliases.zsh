#!/usr/bin/env zsh

source "${HOME}/.zshrc"
print -lr - ${(k)aliases} ${(k)commands} ${(k)functions} | dmenu -i "$@"
