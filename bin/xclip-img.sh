#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# input=$1
# mimetype=$(file -b --mime-type "$input")

# xclip -selection clipboard -t "$mimetype" -i "$input"
# # xclip -selection clipboard -t "$mimetype"  < "$input"

command -v xclip >/dev/null 2>&1 || { echo "Need command xclip. Aborting." >&2; exit 1; }
[[ -f "$1" ]] || { echo "Error: Not a file." >&2; exit 1; }
TYPE=$(file -b --mime-type "$1")
xclip -selection clipboard -t "$TYPE" < "$1"
