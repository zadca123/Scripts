#!/usr/bin/env bash
set -euo pipefail # bash strict mode

cmd="rofi"
[[ $XDG_SESSION_TYPE=='wayland' ]] && cmd="wofi"

DIR="${SCREENSHOTS:-${HOME}/Pictures}"
[[ ! -d $DIR ]] && mkdir -p "$DIR"
scrot_filename='%Y-%m-%d-%H%M%S_$wx$h.png'
scrot_select="scrot -s -f -l style=dash,width=3,color=cyan"

# options to be displayed
option0="fullscreen"
option1="focused"
option2="selection"
option3="clipboard"
option4="16x9"
option5="4x3"
option6="selection-upload"
options_all="${option0}\n${option1}\n${option2}\n${option3}\n${option4}\n${option5}\n${option6}"

lines=$(echo -e "$options_all" | wc -l)
selected="$(echo -e "$options_all" | $cmd -lines "$lines" -dmenu -p "scrot")"

cd "$DIR"
case $selected in
    $option0) scrot -m "$scrot_filename";;
    $option1) scrot -u "$scrot_filename";;
    $option2 | $option6) sleep 1; $scrot_select "$scrot_filename";;
    $option3) sleep 1; $scrot_select -o "$scrot_filename";;
    $option4) scrot -m -a 320,0,1920,1080 "$scrot_filename";;
    $option5) scrot -m -a 560,0,1440,1080 "$scrot_filename";;
esac

# most recent file in screenshot directory
recent="$(find "$DIR" -type f -printf "%T@ %p\n" | sort -n | cut -d' ' -f 2- | tail -n 1)"
notify-send -i "$recent" "${selected} screenshot taken"

case $selected in
    $option3) xclip -sel c -t image/png "$recent"; sleep 5m && rm "$recent";;
    $option6) upload.sh "$recent";;
esac
