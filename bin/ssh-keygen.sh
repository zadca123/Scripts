#!/usr/bin/env bash
set -euo pipefail

private_key="${1:-$HOME/.ssh/id_rsa}"
public_key="${private_key}.pub"

[[ ! -f $private_key ]] && ssh-keygen -N '' -f "$private_key"


_copy_to_clipboard(){
    xclip -sel c < "$public_key"
}
message(){
    _copy_to_clipboard
    echo "Contents of ${public_key} copied to clipboard"
    cat "$public_key"
    echo "Contents of ${public_key} copied to clipboard"
}


message
