#!/usr/bin/env bash
set -euo pipefail # bash strict mode

debian_version=$(awk -F= '/VERSION_CODENAME/ {print $2}' /etc/os-release)

sudo dpkg --add-architecture i386
sudo mkdir -pm755 /etc/apt/keyrings
sudo wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
sudo wget -NP /etc/apt/sources.list.d/ "https://dl.winehq.org/wine-builds/debian/dists/${debian_version}/winehq-${debian_version}.sources"
sudo apt-get update -y &&
    sudo apt-get install -y --install-recommends winehq-stable
