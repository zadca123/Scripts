#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [Seeding, Idle, 100%, etc...] [transmission-remote flag (--stop, --start, etc...)]"
    exit 2
}

[[ $# -lt 1 ]] && display_help

ids=$(transmission-remote -l | grep "$1" | sed 's/^ *//g' | cut -d' ' -f1 | tr '\n' ',' | tr -d '*')

[[ -n $ids ]] && transmission-remote -t "$ids" "${@:2}"
