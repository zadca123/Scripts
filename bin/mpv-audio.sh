#!/usr/bin/env bash
set -euo pipefail # bash strict mode
notify-send "$TERM"
if [[ $TERM == 'linux' || $TERM == 'dumb' ]]; then
    mpv --volume=50 "$@"
else
    mpv --volume=50 --no-audio-display "$@"
fi
