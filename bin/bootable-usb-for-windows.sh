#!/usr/bin/env bash
set -euo pipefail # bash strict mode
display_help(){
    echo "Usage: ${0##*/} [iso] [/dev/sdX]"
    exit 2
}

[[ $# -ne 2 ]] && display_help

sudo woeusb --target--filesystem NTFS --device "$1" "$2"
