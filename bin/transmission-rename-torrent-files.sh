#!/usr/bin/env bash
set -euo pipefail # bash strict mode
for file in $(find -type f -name "*.torrent"); do
    new_name=$(transmission-show "$file" | grep "^Name: " | cut -d' ' -f2- | tr -d ' -+/\()[]')
    size=$(transmission-show "$file" | grep "Total Size: " | tr -d ' ' | cut -d':' -f2-)
    mv -v "$file" "${new_name}_${size}.torrent"
done
