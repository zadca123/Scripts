#!/usr/bin/env bash
set -euo pipefail # bash strict mode

if command -v rpm; then
    dependencies=(perl-libwww-perl perl-LWP-Protocol-https perl-Data-Dump
                  perl-JSON perl-Gtk3 perl-File-ShareDir perl-Module-Build
                  perl-Term-ReadLine-Gnu perl-JSON-XS perl-Unicode-LineBreak
                  perl-Parallel-ForkManager perl-Memoize)
elif command -v apt; then
    dependencies=(libwww-perl liblwp-protocol-https-perl
                  libdata-dump-perl libjson-perl libmodule-build-perl
                  libgtk3-perl libfile-sharedir-perl
                  libterm-readline-gnu-perl libjson-xs-perl
                  libunicode-linebreak-perl libtext-charwidth-perl)
fi

package-manager.sh install "${dependencies[@]}"

cd /tmp
wget https://github.com/trizen/pipe-viewer/archive/main.zip -O pipe-viewer-main.zip
unzip -n pipe-viewer-main.zip
cd pipe-viewer-main/
perl Build.PL --gtk
# perl Build.PL
sudo ./Build installdeps
sudo ./Build install
