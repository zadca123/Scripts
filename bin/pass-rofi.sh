#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

notify-send "hello"

pushd "${HOME}/.password-store/"
choice="$(find . -type f -name "*.gpg" | wofi -dmenu)"

choice="${choice%*.gpg}"


pass -c "$choice"
