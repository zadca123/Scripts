#!/usr/bin/env bash
set -euo pipefail # bash strict mode
find "$1" -mount -type f -size +50M -exec du -h "{}" \; | sort -n
