#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# The PhotoRec may be useful in your case:

# https://www.cgsecurity.org/wiki/PhotoRec

# https://www.cgsecurity.org/wiki/TestDisk

# Regarding audio files, this is what I would do: Install mplayer (if you don't have it already). It can play most of the A/V formats. If the file is broken, an attempt to play it will produce exit code > 0.

# An example (quick and dirty) approach:

# Run the following one-liner (the example is for .mp3, you can easily modify it for .wav or other formats if you need.
rm ~/bad.lst
find "$PWD" -iname '*.mp3' | while read f; do  echo "mplayer -frames 20 ${f} || echo ${f} >> ~/bad.lst" >> check.sh ; done
# I used -frames 20 option in order to let mplayer to play just a little bit of each file.

# Run the check.sh produced in the previous step:

# sh ./check.sh
# In the bad.lst there will be a list of 'non-playable' files. You can dispose them with another one-liner:

# cat bad.lst | while read f; do mv "${f}" /path/to/wastebin ; done
# or
# cat bad.lst | while read f; do rm "${f}"; done
# Good luck!

# Addendum / Errata
# The corrected version of one-liner from point 1., which will handle complex file names, goes like this:

# find <your_recovery_drive_mount_point> -iname '*.mp3' | while read f; do echo "mplayer -ss 10 -endpos 1  '${f}' | grep -iq failed && echo '${f}' >> bad.lst" >> check.sh ; done
