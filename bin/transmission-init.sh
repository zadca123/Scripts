#!/usr/bin/env bash
set -euo pipefail # bash strict mode
pidof transmission-daemon >/dev/null || (transmission-daemon && notify-send "Starting transmission-daemon...")
