#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# changeBrightness

# Arbitrary but unique message id
msgId="990961"

# Change the Brightness using alsa(might differ if you use pulseaudio)
xbacklight "$@" > /dev/null

########## FIX THIS SCRIPT (ON LAPTOP THIS WILL BE EASIEST TO SETUP)

# Query amixer for the current Brightness and whether or not the speaker is muted
brightness="$(xbacklight)"
mute="$(amixer -c 0 get Master | tail -1 | awk '{print $6}' | sed 's/[^a-z]*//g')"
if [[ $brightness == 0 || "$mute" == "off" ]]; then
    # Show the sound muted notification
    dunstify -a "changeBrightness" -u low -i display-brightness-low "$msgId" "Brightness low"
    # dunstify -a "changeBrightness" -u low -i display-brightness-medium "$msgId" "Brightness medium"
    # dunstify -a "changeBrightness" -u low -i display-brightness-high "$msgId" "Brightness full"
else
    # Show the Brightness notification
    dunstify -a "changeBrightness" -u low -i audio-Brightness-high -r "$msgId" \
        -h int:value:"$brightness" "Brightness: ${brightness}%"
fi

# # Play the Brightness changed sound
# canberra-gtk-play -i audio-Brightness-change -d "changeBrightness"
