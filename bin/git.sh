#!/usr/bin/env bash
set -euo pipefail # bash strict mode

SCRIPT_NAME="${0##*/}"
USER="zadca123"
DATE="$(date +%F_%H%M%S)"
LOG_DIR="${HOME}/.local/share/logs"
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME%%.sh}_${DATE}.log"


display_help() {
    error_message="$*"
    [[ -n ${error_message:-} ]] && echo -e "Error: ${error_message}\n"

    cat <<EOF
Description: multi-managment wrapper for various stuff
Usage: ${SCRIPT_NAME} <C|c|r|f|s|d|...>
Dependencies: git, parallel
l  --show nice logs
C  --clone and enter repository
c  --commit message and push to remote
r  --re_add files in the repository
f  --fetch and pull
s  --search git log history
S  --search for repositories with uncommited changes / unpushed commits to remote
R  --remove file from repository and history (later git push --force-with-lease)
P  --pull all repos
Examples:
 \$ ${SCRIPT_NAME} f
 \$ ${SCRIPT_NAME} S
EOF
    exit 2
}

message(){
    info="$*"
    echo "==== $(date +'%F_%H%M%S') ==== ${info} ====" | tee -a "$LOG_FILE"
}


[[ $# -lt 1 ]] && display_help "No parameters specified"
mkdir -pv "$LOG_DIR"
ln -rsf "$LOG_DIR" "$HOME"


initialize_gitlab(){
    PROJECT="${PWD##*/}"
    if [[ $PROJECT == "$USER" ]]; then
        message "Are you trying to create repo in home directory?"
        exit 2
    fi

    git init
    git add -A
    git commit -m 'Initial commit'
    git push --set-upstream "git@gitlab.com:${USER}/${PROJECT}.git"
    git remote add origin "git@gitlab.com:${USER}/${PROJECT}.git"
    git push origin master
}

re_add(){
    git rm -r --cached . &&  git add -A
}

search_history(){
    git rev-list --all | xargs git grep "$*"
}

commit_and_push(){
    git commit -am "$*" && git push --set-upstream origin "$(git rev-parse --abbrev-ref HEAD)"
}

clone_and_enter(){
    git clone "$@" && cd "${_##*/}";
}

fetch_and_pull(){
    git pull --rebase --autostash
}

search_in_list(){
    git rev-list --all | xargs git grep "$@";
}

pull_all_repos () {
    find "$HOME" -maxdepth 4 -regextype egrep -type d -name .git -not -iregex '.*(trash|cache|plugged).*' -print0 \
        | parallel --tag --keep-order -0 -j99 --ll git -C "{//}" pull --rebase --autostash
}

idk(){
    for i in $(git for-each-ref --format="%(refname:short)" --no-merged=origin/HEAD refs/remotes/origin); do
        git switch --track "$i"
    done
}

rm_file_from_repo_and_history(){
    git filter-branch --prune-empty --index-filter "git rm -rf --cached --ignore-unmatch ${*}" HEAD
}

make_separate_repo(){
    local dir="$1"
    git filter-branch --prune-empty --subdirectory-filter "$dir" -- HEAD
}

git_sparse_clone() {
    local rurl="$1" localdir="$2"
    # git clone "$rurl" --no-checkout
    cd "${rurl##*/}"
    git sparse-checkout init
    git sparse-checkout set "$localdir"
    git checkout
    git pull origin master
}

logs(){
    git log --oneline --decorate --all --graph
}

check_for_unpushed_commits(){
    # Iterate through all branches
    for branch in $(git for-each-ref --format='%(refname:short)' refs/heads/); do
        # Check if the branch has unpushed commits
        if [ "$(git rev-list HEAD...origin/$branch)" ]; then
            message "Branch '${branch}' has unpushed commits."
        fi
    done
}
get_status(){
    DIR="${PWD:-$@}"
    message "Repositories with unpushed commits:"
    find "$DIR" -type d -name '.git' -exec sh -c 'cd "${0}/../" && git status | grep -q "is ahead of" && pwd' "{}" \;
    message "Repositories with changed/deleted/added/untracked files:"
    while read -r repo; do
        repo="${repo//.git/}"
        git -C "$repo" status -s | grep -q -v "^$" && \
            message -e "\n\033[1m${repo}\033[m" && \
            git -C "$repo" status -s || true;
    done < <(find "$DIR" -type d -name '.git')
}

action="$1"
shift
case "$action" in
    l) logs;;
    C) clone_and_enter "$@" ;;
    c) commit_and_push "$@" ;;
    f) fetch_and_pull "$@" ;;
    s) search_history "$@" ;;
    S) get_status "$@" ;;
    r) re_add ;;
    i) initialize_gitlab;;
    I) idk "$@";;
    u) check_for_unpushed_commits ;;
    R) rm_file_from_repo_and_history "$*";;
    P) pull_all_repos ;;
    h|\?|:|*) display_help ;;
esac
