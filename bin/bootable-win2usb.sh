#!/usr/bin/env bash
set -euo pipefail # bash strict mode
set -euxo pipefail

display_help(){
    # echo "Usage: ${0##*/} [flag/s] [file/s]"
    echo "Usage : win2usb.sh VMs/Win10_1803_English_x64.iso /dev/sdX"
    exit 2
}

[[ $# -lt 2 ]] && display_help

# https://www.microsoft.com/software-download/windows10ISO

ISO=$1
USB=$2
FAT="$USB"1

MNT=/mnt/USB
TMP=/mnt/Image

mkdir -p $MNT
mkdir -p $TMP

echo "Creating FAT32 bootable UEFI partition..."
echo -e 'o\nn\np\n1\n\n\nt\nc\na\nw' | fdisk $USB
mkfs.fat $FAT

echo "Copying files..."
mount $FAT $MNT
mount $ISO $TMP
cp -r $TMP/* $MNT
sync
umount $MNT
umount $TMP
echo "Done"
