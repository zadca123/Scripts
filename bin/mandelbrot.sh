#!/usr/bin/env bash
set -euo pipefail # bash strict mode
BAILOUT=16
MAX_ITERATIONS=1000

function iterate {
    # $1 is x
    # $2 is y
    local zi=0
    local zr=0
    local i=0

    local cr
    cr=$(printf "%s\n" "scale=16; $2 - 0.5" | bc)

    while true
    do
        local temp
        local zr2
        local zi2
        i=$((i + 1))
        zr2=$(printf "%s\n" "scale=16; ($zr * $zr) - ($zi * $zi) + $cr" | bc)
        zi2=$(printf "%s\n" "scale=16; (($zr * $zi) * 2) + $1" | bc)
        temp=$(printf "%s\n" "(($zi * $zi) + ($zr * $zr)) > $BAILOUT" | bc)

        if ((temp == 1))
        then
            return "$i"
        fi

        if ((i > MAX_ITERATIONS))
        then
            return 0
        fi

        zr="$zr2"
        zi="$zi2"
    done
}

function mandelbrot {
    local y
    for ((y = -39; y < 39; y++))
    do
        printf "\n"
        local x
        for ((x = -39; x < 39; x++))
        do
            local xi
            local yi
            local ires
            xi=$(printf "%s\n" "scale=16; $x / 40.0" | bc)
            yi=$(printf "%s\n" "scale=16; $y / 40.0" | bc)
            iterate "$xi" "$yi"
            ires=$?

            if ((ires == 0))
            then
                printf "*"
            else
                printf " "
            fi
        done
    done
    printf "\n"
}

mandelbrot
