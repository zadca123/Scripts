#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
SCRIPT_NAME="${0##*/}"


display_help() {
    echo "Usage: ${SCRIPT_NAME} <dir1> <dir2>"
    echo "Dependencies: tree diff"
    echo "Example:"
    echo "- ${SCRIPT_NAME} ~/Downloads ~/Downloads_backup"
    echo "- ${SCRIPT_NAME} ./git_repo1 ./git_repo2"
    exit 2
}


[[ $# -ne 2 || ! -d $1 || ! -d $2 ]] && display_help

tree_file="/tmp/${SCRIPT_NAME}_tree"

tree_in_dir(){
    cd "$1"
    tree -dfpiug
}


declare -i index=1
for dir in "$@"; do
    tree_in_dir "$dir" > "${tree_file}_${index}"
    ((index+=1))
done

diff --color "$tree_file"*

# diff_tree(){
#     diff --color \
#          <(cd "$dir1"; $tree_cmd) \
#          <(cd "$dir2"; $tree_cmd)
# }


# diff_tree
