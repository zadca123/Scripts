#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# for mu4e
if ! command -v mu; then
    if command -v rpm; then
        package-manager.sh install maildir-utils-guile isync
    elif command -v apt; then
        package-manager.sh install libgmime-3.0-dev libxapian-dev guile-2.2-dev html2text xdg-utils meson isync
        git clone https://github.com/djcb/mu.git /tmp/mu
        cd /tmp/mu
        ./autogen.sh && make
        sudo make install
    fi
fi

# idk if its not automatically done by mu4e
maildir="${HOME}/.mail"
mkdir -p "$maildir"
read -erp "Type your email address: " -i "example1@mailo.com" mail
mu init --maildir="${maildir}" --my-address="${mail}"
mu index
