#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# # TODO
# ## Make this script works with other archive protocols like zst lzma

# if [[ $1 == *'.gpg' ]]; then
#     gpg -d "$1"  | tar -xzvpf -
#     exit
# fi

# sudo tar -czvpf - \
    #      /etc/fstab \
    #      ~/org \
    #      ~/Documents/{org} \
    #      ~/.icons \
    #      ~/.var \
    #      ~/.*{wm,rc,conf,_profile,env,.d,moonchild*,gtkrc*} \
    #      ~/.local/{bin,share/applications/*.desktop} \
    #      ~/.config/{cmus,*wm,i3*,neofetch,zathura,vifm,nvim,mimeapps.list} \
    #      ~/.config/{rofi,mpv,aerc,alacritty,geany,strawberry,pcmanfm,transmission*,awesome,qtile} \
    #      ~/.config/{dunst,rox*,qutebrowser,falkon} \
    #     | gpg --symmetric --cipher-algo aes256 -o "${arc}.gpg"

####################################################################################
dest="${HOME}/Dotfiles"

rsync -ruRmP --mkpath --max-size=2m \
    ~/.icons \
    ~/.*{wm,rc,conf,_profile,env,gtkrc*} \
    ~/.local/{org,share/applications/*.desktop} \
    ~/.config/{*wm,i3*,rox*} \
    ~/.config/{neofetch,zathura,vifm,nvim,mimeapps.list,cmus,rofi,mpv/mpv.conf,aerc,alacritty,geany,strawberry,pcmanfm,transmission*/*.json,awesome,qtile} \
    ~/.config/{dunst,rox*,qutebrowser} \
    "/tmp"

# rm -rf "$dest"
cp -r "/tmp/home/${USER}/" "$dest/"

# arc="backup-configs_$(date +'%Y-%m-%d').tar.gz"
# tar -czvpf "$dest" "$arc"
