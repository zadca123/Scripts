#!/usr/bin/env bash
set -euo pipefail # bash strict mode

if (command -v cpg && command -v mvg) >/dev/null ; then
    echo "Advanced commands 'cpg' and 'mvg' are already installed!"
else
    package-manager.sh install patch make gcc
    curl https://raw.githubusercontent.com/jarun/advcpmv/master/install.sh \
         --create-dirs -o ./advcpmv/install.sh && (cd advcpmv && sh install.sh)

    sudo mv ./advcpmv/advcp /usr/local/bin/cpg
    sudo mv ./advcpmv/advmv /usr/local/bin/mvg
    rm -rf ./advcpmv
fi
