#!/usr/bin/env bash
set -euo pipefail # bash strict mode
ffmpeg -i "$1" -filter_complex "[0:a]showwaves=s=1280x720:mode=line:rate=25, format=yuv420p[v]" -map "[v]" -map 0:a "${1%.*}.${2:-mp4}"

# https://blog.grio.com/2016/07/fractals-and-music-visualization-with-ffmpeg.html

# [code language=”bash”]ffmpeg -i spectrum.mkv -vf drawtext="fontsize=50:fontfile=/Library/Fonts/Arial.ttf:text=’Wolf-e-Wolf – Purple Planet’:fontcolor=black:x=(w-text_w)/2:y=h-(2*text_h)" -b 2048k text-hd.avi[/code]

# [code language=”bash”]ffmpeg -i high-adventure.mp3 -filter_complex "[0:a]showspectrum=s=hd1080[v]" -map "[v]" -map 0:a -c:v libx264 -c:a copy spectrum.mkv[/code]

# [code language=”bash”]ffmpeg -i high-adventure.mp3 -filter_complex "[0:a]showspectrum=s=1280×720,format=yuv420p[v]" -map "[v]" -map 0:a -c:v libx264 -c:a copy spectrum.mkv[/code]

# [code language=”bash”]FFmpeg -i Imagine\ The\ Future.mp3 -i mandelbrot.mkv -b 2048k -to 03:57 mandelbrot-imagine-short.avi[/code]

# [code language=”shell”]ffmpeg -f lavfi -i mandelbrot mandelbrot.mkv[/code]

# # https://video.stackexchange.com/questions/9644/how-do-i-turn-audio-into-video-that-is-show-the-waveforms-in-a-video

# You can use ffmpeg to create video from audio using several filters.
# ahistogram

# Convert input audio to a video output, displaying the volume histogram.

# ahistogram

# ffmpeg

# ffmpeg -i input.flac -filter_complex \
    # "[0:a]ahistogram,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.flac, asplit [a][out1]; [a] ahistogram [out0]"

# See the ahistogram documentation for more options and examples.
# aphasemeter

# Convert input audio to a video output, displaying the audio phase.

# aphasemeter

# ffmpeg

# ffmpeg -i input.wav -filter_complex \
    # "[0:a]aphasemeter=s=1280x720:mpc=cyan,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.wav, asplit [a][out1]; [a] aphasemeter=s=1280x720:mpc=cyan [out0]"

# See the aphasemeter documentation for more options and examples.
# avectorscope

# avectorscope filter

# Convert input audio to a video output, representing the audio vector scope.

# ffmpeg

# ffmpeg -i input.mp3 -filter_complex \
    # "[0:a]avectorscope=s=1280x720,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.mp3, asplit [a][out1]; \
    # [a] avectorscope=zoom=1.3:rc=2:gc=200:bc=10:rf=1:gf=8:bf=7 [out0]"

# See the avectorscope documentation for more options and examples.
# showcqt

# showcqt filter

# Convert input audio to a video output representing frequency spectrum with musical tone scale.

# ffmpeg

# ffmpeg -i input.mp4 -filter_complex \
    # "[0:a]showcqt,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.mp4, asplit [a][out1]; [a] showcqt [out0]"

# See the showcqt documentation for more options and examples.
# showfreqs

# Convert input audio to video output representing the audio power spectrum. Audio amplitude is on Y-axis while frequency is on X-axis.

# showfreqs

# ffmpeg

# ffmpeg -i input.mp4 -filter_complex \
    # "[0:a]showfreqs=mode=line:fscale=log,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.mp4, asplit [a][out1]; [a]  showfreqs=mode=line:fscale=log [out0]"

# See the showfreqs documentation for more options and examples.
# showspectrum

# showspectrum image

# Convert input audio to a video output, representing the audio frequency spectrum.

# ffmpeg

# ffmpeg -i input.oga -filter_complex \
    # "[0:a]showspectrum=s=1280x720,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.oga, asplit [a][out1]; \
    # [a] showspectrum=mode=separate:color=intensity:slide=1:scale=cbrt [out0]"

# See the showspectrum documentation for more options and examples.
# showwaves

# showwaves filter

# Convert input audio to a video output, representing the samples waves.

# ffmpeg

# ffmpeg -i input.m4a -filter_complex \
    # "[0:a]showwaves=s=1280x720:mode=line:rate=25,format=yuv420p[v]" \
    # -map "[v]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.m4a, asplit [a][out1]; [a] showwaves [out0]"

# See the showwaves documentation for more options and examples.
# showvolume

# showvolume filter

# Convert input audio volume to a video output.

# ffmpeg

# ffmpeg -i input.mka -filter_complex \
    # "[0:a]showvolume=f=1:b=4:w=720:h=68,format=yuv420p[vid]" \
    # -map "[vid]" -map 0:a output.mp4

# ffplay

# ffplay -f lavfi "amovie=input.mka, asplit [a][out1]; [a] showvolume=f=255:b=4:w=720:h=68 [out0]"

# See the showvolume documentation for more options and examples.
