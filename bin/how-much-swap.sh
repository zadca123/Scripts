#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Function to calculate swap space
calculate_swap() {
    local ram=$1

    if (( ram <= 2 )); then
        echo "$((ram * 2)) GB"
    elif (( ram > 2 && ram <= 8 )); then
        echo "$ram GB"
    elif (( ram > 8 && ram <= 64 )); then
        echo "4 GB"
    else
        echo "4 GB (Hibernation not recommended)"
    fi
}

# Main script
echo "Enter the amount of RAM in GB: "
read ram_amount

# Check if input is numeric
if ! [[ "$ram_amount" =~ ^[0-9]+$ ]]; then
    echo "Error: Input must be a positive integer."
    exit 1
fi

# Call function to calculate swap space
swap_space=$(calculate_swap $ram_amount)
echo "Recommended swap space: ${swap_space}"
