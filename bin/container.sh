#!/usr/bin/env bash
set -euo pipefail # bash strict mode

SCRIPT_NAME="${0##*/}"
ENGINE="${ENGINE:-docker}"
display_help(){
    echo "Usage: ${SCRIPT_NAME} <image[:tag]> [rest-docker|podman-options]"
    echo "Example: ${SCRIPT_NAME} debian:bookworm-slim"
    echo "Example: ${SCRIPT_NAME} archlinux -v './mydir:/data:ro'"
    echo "Example: ENGINE=podman ${SCRIPT_NAME} python:3.12"
    exit 2
}

[[ $# -lt 1 ]] && display_help
echo "Using ENGINE=${ENGINE}"


IMAGE="$1"
PARAMS="${*:2}"
NAME="${IMAGE//[^A-z0-9]/_}"


search_for_image(){
    echo "Searching for official image with name ${NAME}"
    docker search --filter=is-official=true "$NAME"
}
local_container_exists(){
    $ENGINE ps -a | grep -q " ${NAME} " # spaces are required
}
container_up(){
    $ENGINE ps | grep -q "$NAME"
}
container_stop(){
    detach.sh $ENGINE container stop "$NAME"
    notify-send "$SCRIPT_NAME" "Stopping ${NAME}"
    while container_up; do
        sleep 5
    done
    notify-send "$SCRIPT_NAME" "${NAME} stopped successfully"
}
local_container_run(){
    $ENGINE container start "$NAME"
    $ENGINE exec -it "$NAME" bash

    # it will run if you exit container with C-d, C-c will cancell script
    container_stop &
}
container_create(){
    $ENGINE run -it $PARAMS --name "$NAME" "$IMAGE" bash
}


if local_container_exists; then
    local_container_run
else
    container_create || search_for_image
fi
