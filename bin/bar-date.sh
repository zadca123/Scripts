#!/usr/bin/env bash
set -euo pipefail # bash strict mode
# date="$(date +"%a, %B %d %l:%M:%S %p"| sed 's/  / /g')"
date=$(date +'%A %d-%m-%Y [%H:%M]')
echo -e "${date}"
