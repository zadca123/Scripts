#!/usr/bin/env bash
set -euo pipefail # bash strict mode
[[ -f "$2" ]] && 2=$(cat "$2")
# sed -i.bak '/l6:6:wait:\/etc\/rc.d\/rc 6/ a \/sbin\/if-pp-to-cng' /etc/inittab
sed -i.bak "/${1}/ a ${2}" "${@:3}"
# sed "${line_index} c${new_value}" "$i"

# awk -v new="/sbin/if-pp-to-cng" '($0=="l6:6:wait:/etc/rc.d/rc 6") {$0=$0"\n"new} 1' /etc/inittab
# # Why use a variable, add it directly
# awk '($0=="l6:6:wait:/etc/rc.d/rc 6") {$0=$0"\n/sbin/if-pp-to-cng"} 1' /etc/inittab
