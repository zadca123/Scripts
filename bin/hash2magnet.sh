#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help() {
    echo "Usage: ${0##*/} [hash/es]"
    echo "Scripts converts hash to magnetlink"
    exit 2
}
[[ $# -lt 1 ]] && display_help

for hash in "$@"; do
    echo "magnet:?xt=urn:btih:${hash}"
done
