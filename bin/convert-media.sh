#!/usr/bin/env bash
set -euo pipefail # bash strict mode
#converts video or audio file with any extension to extension precised with first parameter
display_help(){
    echo "Usage: ${0##*/} -e [extension] -a [audio-bitrate] -v [video-bitrate] [file/s]"
    echo "Example: ${0##*/} -e opus Despacito.flac GangnamStype.wav"
    echo "Example: ${0##*/} -e webm Video1.mp4 *.mkv"
    exit 2
}

[[ "$#" -lt 2 ]] && display_help

while getopts 'a:v:e:h' opt; do
    case "$opt" in
        a) output_opt+=(-b:a "${OPTARG}") ;;
        v) output_opt+=(-b:v "${OPTARG}") ;;
        e) ext="$OPTARG" ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

for inputf in "$@";do
    outputf="${inputf%.*}.${ext}"
    ffmpeg -y -i "$inputf" -vsync 2 "${output_opt[@]}" "$outputf"
done
