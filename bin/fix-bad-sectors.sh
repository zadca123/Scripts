#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# The script below finds bad sectors on ALL discs,
# puts them into a text file,
# then test if text file size is different than zero,
# so e2fsck will mark bad sectors
# (these sectors will not be used by operating system)

minsize=0
target="/tmp/bad-blocks.txt"
for disc in `fdisk -l | grep '^/' | awk '{ print $1 }'`; do
    badblocks -v $disc > "$target"
    tam=$(du -k "$target" | cut -f 1)
    if [ $tam -eq $minsize ]; then
        echo "no badblocks on $disc"
    else
        echo "badblock(s) found(s) on $disc"
        e2fsck -l "$target" "$disc"
    fi
done
