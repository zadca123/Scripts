#!/usr/bin/env bash
set -euo pipefail # bash strict mode

wget "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts" -O /tmp/hosts
