#!/usr/bin/env bash
set -euo pipefail # bash strict mode

dl() {
    if [[ -z "${1:-}" ]]; then
        first="1"
    else
        first="$1"
    fi

    if [[ -z "${2:-}" ]]; then
        last="2"
    else
        last="$2"
    fi
    # shellcheck disable=SC2091
    diff --color <( $(fc -ln "-${first}" "-${first}") ) <( $(fc -ln "-${last}" "-${last}") )
}

dl
