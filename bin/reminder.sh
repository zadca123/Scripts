#!/usr/bin/env bash
set -euo pipefail # bash strict mode
while :; do
    figlet "$*"
    notify-send "$*"
    sleep 10m
done
