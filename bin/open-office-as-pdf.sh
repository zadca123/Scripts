#!/usr/bin/env bash
set -euo pipefail # bash strict mode
libreoffice \
    --headles \
    --invisible \
    --convert-to pdf "$1" \
    --outdir /tmp

zathura "/tmp/${1##*/}"
