#!/usr/bin/env bash
set -euo pipefail # bash strict mode


# defaults for easy creating webm to 4chan
INPUT_OPTS=(-y)
OUTPUT_OPT=(-c:v libvpx-vp9 -c:a libvorbis -sn -b:a 48k -b:v 320k -fs 6000000)
EXT="webm"
CUSTOM_FN=""


display_help(){
    echo "Usage: ${0##*/} <file/s>"
    echo "Default behaviour: makes webm from audio or video"
    echo "Where:"
    echo " -a  --Specifies audio bitrate"
    echo " -v  --Specifies video bitrate"
    echo " -s  --Specifies webm max size"
    echo " -d  --Scale down webm"
    echo " -b  --Start from time (can be in seconds or HH:MM:SS)"
    echo " -e  --End to time (can be in seconds or HH:MM:SS)"
    echo " -t  --Duration time"
    echo " -c  --Compression level (set number between 4 and 63, smaller number==better quality but larger filesize)"
    echo " -o  --Custom output file"
    echo " -C  --Copy audio and video codecs"
    echo " -N  --No audio (SPECIFY AT THE BEGGINIG)"
    echo " -O  --Output filename with current date and time"
    echo " -R  --Reverse video and audio (not recommended on large/long files)"
    echo " -V  --Reverse video"
    echo " -A  --Reverse audio"
    echo " -F  --Speedup webm 2 times"
    echo " -S  --Slowdown webm 2 times"
    echo " -1  --Change speed of video (float values from 0 to 2)"
    echo " -2  --Change speed of audio (float values from 0.5 to 2)"
    echo ""
    echo "Default or specified OUTPUT_OPT:"
    echo "\$ ffmpeg ${INPUT_OPTS[*]} -i $input_file ${OUTPUT_OPT[*]} ${output_file}"
    exit 2
}



while getopts 'a:v:s:d:c:b:t:o:e:1:2:NOFRSVAh' opt; do
    case "$opt" in
        a) OUTPUT_OPT+=(-b:a "${OPTARG}") ;;
        v) OUTPUT_OPT+=(-b:v "${OPTARG}") ;;
        s) OUTPUT_OPT+=(-fs "${OPTARG}") ;; # TODO if filesize hits limit but the time not, create new webm from the stop time?
        d) OUTPUT_OPT+=(-vf "scale=iw/${OPTARG}:ih/${OPTARG}") ;;
        c) OUTPUT_OPT+=(-crf "${OPTARG}") ;;
        b) INPUT_OPTS+=(-ss "${OPTARG}") ;; # TODO -e end flag ffmpeg -to
        e) INPUT_OPTS+=(-to "${OPTARG}") ;; # TODO -e end flag ffmpeg -to
        t) OUTPUT_OPT+=(-t "${OPTARG}") ;;
        o) CUSTOM_FN="$OPTARG" ;; # TODO
        1) OUTPUT_OPT+=(-filter:v "setpts=${OPTARG}*PTS") ;;
        2) OUTPUT_OPT+=(-filter:a "atempo=${OPTARG}") ;;
        N) OUTPUT_OPT+=(-c:v libvpx -map 0:0) ;; # if something is wrong maybe delete + from this line
        O) CUSTOM_FN="CURRENT_DATE" ;; # TODO
        F) OUTPUT_OPT+=(-filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]" -map "[v]" -map "[a]") ;;
        S) OUTPUT_OPT+=(-filter_complex "[0:v]setpts=2.0*PTS[v];[0:a]atempo=0.5[a]" -map "[v]" -map "[a]") ;;
        R) OUTPUT_OPT+=(-vf reverse -af areverse) ;;
        V) OUTPUT_OPT+=(-vf reverse) ;;
        A) OUTPUT_OPT+=(-af areverse) ;;
        h|\?|:|*) display_help ;;
    esac
done
shift $((OPTIND-1))

create_dirs(){
    file="$1"
    dir=$(dirname "$file")
    mkdir -p "$dir"
}
embed_subtitles(){
    input_file="$1"
    subtitle_file="${input_file%.*}.srt"
    [[ -f $subtitle_file ]] && OUTPUT_OPT+=(-vf "subtitles=${subtitle_file}")
}
run_ffmpeg(){
    input_file="$1"
    ffmpeg "${INPUT_OPTS[@]}" \
           -i "$input_file" \
           "${OUTPUT_OPT[@]}" \
           -threads 4 \
           -max_muxing_queue_size 9999 \
           "$output_file"
}

[[ "$#" -lt 1 ]] && display_help

# variable for make an index during specyfying custom filename (one) for multiple files
nr=1
for input_file in "$@"; do

    filetype=$(file --mime-type -b "$input_file" | cut -d/ -f1)
    case "$filetype" in
        video) dir="${HOME}/Documents/4chan/video" ;;
        audio) dir="${HOME}/Documents/4chan/audio" && OUTPUT_OPT+=(-r 1) ;;
        *) echo "Wrong filetype: ${filetype}"
           echo "File should be of 'audio' or 'video' filetype"
           continue ;;
    esac
    mkdir -p "$dir"

    if [[ $CUSTOM_FN == "CURRENT_DATE" ]]; then # specified -O flag
        output_file="${dir}/$(date +'%Y-%m-%d-%H%M%S')_${input_file%.*}.${EXT}"
    elif [[ -n $CUSTOM_FN ]]; then #specified -o flag
        output_file="${dir}/${CUSTOM_FN%.*}-${nr}.${EXT}"
        (( nr+=1 ))
    else
        output_file="${dir}/${input_file%.*}.${EXT}"
    fi

    create_dirs "$output_file"
    # embed_subtitles "$input_file"
    run_ffmpeg "$input_file"
done

# path of the clip goes to the clipboard
echo "$output_file" | xclip -selection clipboard

# # some guy from 4chan said...
# ffmpeg -i INPUT -c:v libvpx -crf 4 -b:v 1500K -vf scale=320:-1 -an output.webm
