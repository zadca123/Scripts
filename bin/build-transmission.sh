#!/usr/bin/env bash
set -euo pipefail # bash strict mode
set -euxo pipefail

install_dependencies(){
    sudo apt-get install -y \
         git \
         build-essential \
         cmake \
         libcurl4-openssl-dev \
         libssl-dev \
         python3 \
         libgtkmm-3.0-dev \
         gettext \
         libappindicator3-0.1-cil-dev
}
build_transmission(){
    dir=$(find -maxdepth 1 -type d -name 'transmission-*' | head -n1)
    cd "$dir"
    mkdir -p build && cd build
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo .. -DENABLE_GTK=ON
    make
    sudo make install
}
download_latest_release(){
    latest_release="https://github.com/transmission/transmission/releases/latest"
    latest_tag=$(curl -I "$latest_release" | grep 'location: ' | tr -d '\r')
    latest_tag="${latest_tag##*/}"
    archive="transmission-${latest_tag}.tar.xz"
    wget "${latest_release}/download/${archive}"
    tar xvf "$archive"
}
download_latest_release
install_dependencies
build_transmission
