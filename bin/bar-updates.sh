#!/usr/bin/env bash
set -euo pipefail # bash strict mode
cupd=$(checkupdates | wc -l)
echo "$cupd updates "
