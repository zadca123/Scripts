#!/usr/bin/env bash
set -euo pipefail # bash strict mode
dte=$(date +'%Y-%m-%d')
path="${1:-/mnt/crucial_nvme/Backups}"
archive="${path}/backup_${dte}.tar.${2:-xz}"
export XZ_OPT=-9v

tar --ignore-case \
    --exclude=/{dev,proc,sys,run,tmp,opt,mnt,media} \
    --exclude={swapfile,share,docker,flatpak} \
    --exclude=*{cache,lost-found,trash,steam}* \
    --exclude=.{local,wine,mozilla} \
    --exclude=*.{mp4,mkv,flac,tar}* \
    --exclude-backups --exclude-caches-all \
    -acpvf "$archive" /
