#!/usr/bin/env bash
set -euo pipefail
SCRIPT_NAME="${0##*/}"
# # i have this defined in ~/.bash_profile
# FZF_DEFAULT_COMMAND="locate \$(pwd)"; export FZF_DEFAULT_COMMAND
# export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"


display_help() {
    echo "Usage: ${SCRIPT_NAME} <action>"
    echo "Dependencies: fzf fzy"
    echo "Examples:"
    echo "- ${SCRIPT_NAME} run"
    echo "Available actions:"
    echo "- run"
    exit 2
}


fzf_cmd(){
    file="$(fzf)"
    # file="$(find -mount | fzy)"

    if [[ -f $file && $(file -b --mime-type "${file}") == 'text/plain' ]]; then
        cd "$(dirname "${file}")"
        $EDITOR "$file"
    elif [[ -d $file ]]; then
        cd "$file"
    fi


    file "$file"
}


if [[ "${1:-}" == 'run' ]]; then
    fzf_cmd
else
    display_help
fi
