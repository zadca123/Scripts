#!/usr/bin/env bash
set -euo pipefail # bash strict mode
while true; do
    xsetroot -name "$(bar-cmus.sh) | $(bar-volume.sh) | $(bar-cpu.sh) | $(bar-memory.sh) | $(bar-bandwidth.sh) | $(bar-upt.sh) | $(bar-date.sh)"
    sleep 5s
done
