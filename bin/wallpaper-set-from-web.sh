#!/usr/bin/env bash
set -euo pipefail # bash strict mode

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # Get the script's current directory

linksFile="links"

mkdir "${DIR}/downloads"
cd "${DIR}/downloads"

# Strip the image links from the html
parse(){
    grep -o -E 'href="([^"#]+)"' "$1" | cut -d'"' -f2 | sort | uniq | grep -E '.jpg|.png' >> temp
    grep -o -E 'href="([^"#]+)"' "$2" | cut -d'"' -f2 | sort | uniq | grep -E '.jpg|.png' >> temp
    grep -o -E 'href="([^"#]+)"' "$3" | cut -d'"' -f2 | sort | uniq | grep -E '.jpg|.png' >> temp
    grep -o -E 'href="([^"#]+)"' "$4" | cut -d'"' -f2 | sort | uniq | grep -E '.jpg|.png' >> temp
}

# Download the subreddit's webpages
function download {
    rname=$( echo "$1" | cut -d / -f 5  )
    tname=$(echo t."$rname")
    rrname=$(echo r."$rname")
    cname=$(echo c."$rname")
    wget --load-cookies=../cookies.txt -O "$rname" "$1" &>/dev/null &
    wget --load-cookies=../cookies.txt -O "$tname" "$1"/top &>/dev/null &
    wget --load-cookies=../cookies.txt -O "$rrname" "$1"/rising &>/dev/null &
    wget --load-cookies=../cookies.txt -O "$cname" "$1"/controversial &>/dev/null &
    wait # wait for all forked wget processes to return
    parse "$rname" "$tname" "$rrname" "$cname"
}

# For each line in links file
while read l; do
    if [[ "$l" != *"#"* ]]; then # if line doesn't contain a hashtag (comment)
        download "$l"&
    fi
done < "../${linksFile}"

wait # wait for all forked processes to return

sed -i '/www.redditstatic.com/d' temp # remove reddit pics that exist on most pages from the list

wallpaper=$(shuf -n 1 temp) # select randomly from file and DL

echo "$wallpaper" >> "$DIR"/log # save image into log in case we want it later

wget -b "$wallpaper" -O "$DIR"/wallpaperpic 1>/dev/null # Download wallpaper image

# gsettings set org.gnome.desktop.background picture-uri file://"$DIR"/wallpaperpic # Set wallpaper (Gnome only!)
feh "${DIR}/wallpaperpic" --bg-fill

rm -r "${DIR}/downloads" # cleanup
