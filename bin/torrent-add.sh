#!/usr/bin/env bash
set -euo pipefail # bash strict mode

magnet2torrent(){
    notify-send "Started aria2c" "$1"
    aria2c -d "${HOME}/Downloads/" --bt-metadata-only=true --bt-save-metadata=true "$1"
}

hostname="192.168.0.72"

add_torrent(){
    # # NOT WORKS (IT WORKED FOR RTORRENT)
    # [[ "$1" =~ xt=urn:btih:([^&/]+) ]] || exit;
    # echo "d10:magnet-uri${#1}:${1}e" >
    # "${HOME}/Downloads/meta-${BASH_REMATCH[1]}.torrent"

    if ping -c3 -w5 "$hostname"; then
        transmission-remote "$hostname" -a "$1"
    else
        hostname="aria2c"
        aria2c --daemon "$1"
    fi
}
notification(){
    notify-send "Torrent added to ${hostname}" "$torrent"
}

for torrent in "$@"; do
    add_torrent "$torrent" && notification
done
