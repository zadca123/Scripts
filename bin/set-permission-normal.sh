#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# Set the desired permissions for files and directories
file_permissions=644
directory_permissions=755

# Find all files and directories recursively
while IFS= read -red '' file; do
    if [[ -f $file ]]; then
        chmod -v "$file_permissions" "$file"
    elif [[ -d $file ]]; then
        chmod -v "$directory_permissions" "$file"
    else
        echo "Skipping: ${file} is not a regular file or directory"
    fi
done < <(find . -print0)
