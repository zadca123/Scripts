#!/usr/bin/env bash
set -euo pipefail # bash strict mode

[[ ! -f /bin/apt ]] && exit 2

sudo apt install clang make cmake asciidoc libx11 \
    xft freetype xrandr xinerama xsetroot xterm dzen2 dmenu

git clone https://github.com/herbstluftwm/herbstluftwm
cd herbstluftwm
make # build the binaries

# install files
mkdir -p ~/.local/bin
# you also have to put $HOME/bin to your path, e.g. by:
# echo "export PATH=$PATH:$HOME/bin" >> ~/.bashrc # or to your zshrc, etc...
ln -s "$(pwd)/herbstluftwm" ~/.local/bin/
ln -s "$(pwd)/herbstclient" ~/.local/bin/

# copy the configuration
mkdir -p ~/.config/herbstluftwm/
cp share/autostart ~/.config/herbstluftwm/
cp share/panel.sh ~/.config/herbstluftwm/
