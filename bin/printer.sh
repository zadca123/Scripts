#!/usr/bin/env bash
set -euo pipefail # bash strict mode

IFS=$'\n\t' # Internal Field Separator, controls word splitting, default is $' \n\t'
# set -e # exit if any command has a non-zero exit status
# set -u # exit if not defined variable is referenced
# set -o pipefail # prevents errors in a pipeline from being masked

display_help(){
    echo "Usage: ${0##*/} [arg/s]"
    exit 2
}
error() {
    echo "$0: $*" >&2
    exit 1
require_sudo(){
    if [[ $EUID -ne 0 ]]; then
        echo "Please run as superuser! Try running:"
        echo "sudo ${0}"
        exit 2
    fi
[[ $# -lt 1 ]] && display_help
scanimage --format=pdf > eeee.pdf
