#!/usr/bin/env bash
set -euo pipefail # bash strict mode

display_help(){
    echo "Usage: ${0##*/} [time] [message/s]"
    echo "Flags:"
    echo -e "\t -h --Display help message"
    exit 2
}

[[ $# -lt 1 ]] && display_help

time="${1:-now}"
message="${@:2}"

echo "notify-send '${message}'" | at "${time}"
