#!/usr/bin/env bash
set -euo pipefail # bash strict mode
string="$*"
output=""

for (( i=0; i<${#string}; i++ )); do
    char=${string:$i:1}
    octal=$(printf '%03o' "'$char")
    output="$output\\$octal"
done

echo "\$'${output}'"
