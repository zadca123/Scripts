#!/usr/bin/env bash
set -euo pipefail # bash strict mode
msgId="291011"

if [[ ! $(pidof mocp) ]]; then
    echo "mocp is not running..."
    exit 2
fi

cover-music-generate.sh "$(mocp --format %file)" "${1:-200}"
sleep 0.2

tags=$(mocp --info | sed '11d;10d;9d;3d;2d' | \
    sed 's/ /\t/' | column -t -s $'\t')
title='mocp-notifier'
notify-send -i "/tmp/cover.png" "$title" "$tags"
# dunstify -i "/tmp/cover.png" -r "$msgId" "$title" "$tags"

# set this in moc config
#OnSongChange = /absolute/path/moc-notify.sh
#RepeatSongChange = yes
