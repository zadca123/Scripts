#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# ways to switch keyboard
# grep "grp:.*toggle" /usr/share/X11/xkb/rules/base.lst
#   grp:toggle           Right Alt
#   grp:lalt_toggle      Left Alt
#   grp:caps_toggle      Caps Lock
#   grp:shift_caps_toggle Shift+Caps Lock
#   grp:alt_caps_toggle  Alt+Caps Lock
#   grp:shifts_toggle    Both Shift together
#   grp:alts_toggle      Both Alt together
#   grp:ctrls_toggle     Both Ctrl together
#   grp:ctrl_shift_toggle Ctrl+Shift
#   grp:lctrl_lshift_toggle Left Ctrl+Left Shift
#   grp:rctrl_rshift_toggle Right Ctrl+Right Shift
#   grp:ctrl_alt_toggle  Alt+Ctrl
#   grp:alt_shift_toggle Alt+Shift
#   grp:lalt_lshift_toggle Left Alt+Left Shift
#   grp:alt_space_toggle Alt+Space
#   grp:menu_toggle      Menu
#   grp:lwin_toggle      Left Win
#   grp:win_space_toggle Win+Space
#   grp:rwin_toggle      Right Win
#   grp:lshift_toggle    Left Shift
#   grp:rshift_toggle    Right Shift
#   grp:lctrl_toggle     Left Ctrl
#   grp:rctrl_toggle     Right Ctrl
#   grp:sclk_toggle      Scroll Lock
#   grp:lctrl_lwin_toggle Left Ctrl+Left Win

way_to_toggle="grp:win_space_toggle"
keyboard_layout="pl,ru"
keyboard_model="pc105"
keyboard_variant=",phonetic"

setxkbmap -model "${keyboard_model}" -layout "${keyboard_layout}" -variant "${keyboard_variant}" -option "${way_to_toggle}"
localectl set-x11-keymap "${keyboard_layout}" "${keyboard_model}" "${keyboard_variant}" "${way_to_toggle}"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'pl'), ('xkb', 'ru+phonetic')]"
gsettings set org.gnome.desktop.input-sources xkb-options "['${way_to_toggle}']"

sudo tee /etc/X11/xorg.conf.d/00-keyboard.conf > /dev/null <<EOF
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "${keyboard_layout}"
        Option "XkbModel" "${keyboard_model}"
        Option "XkbVariant" "${keyboard_variant}"
        Option "XkbOptions" "${way_to_toggle}"
EndSection
EOF
