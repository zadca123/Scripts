#!/usr/bin/env bash
set -euo pipefail # bash strict mode
SCRIPT_NAME="${0##*/}"
DIR="/tmp/${SCRIPT_NAME}"
DATE="$(date +%s)"
CLIP_FILE="${DIR}/clipboard_${DATE}"
URL_HIST_FILE="${DIR}/history"


display_help() {
    echo "Dependencies: curl xclip"
    echo "Example:"
    echo "- ${SCRIPT_NAME} ./path/to/file.{txt,pdf}"
    echo "- ${SCRIPT_NAME} clipboard"
    exit 2
}


[[ $# -lt 1 ]] && display_help


file="$1"
mkdir -p "$DIR"
if [[ $file == 'clipboard' ]]; then
    file="$CLIP_FILE"
    xclip -o -sel clip > "$file"
fi


path=$(realpath "$file")
# url=$(curl -s -F "reqtype=fileupload" -F "fileToUpload=@${path}" https://catbox.moe/user/api.php)
url=$(curl -s -F "reqtype=fileupload" -F "fileToUpload=@${path}" -F "time=24h" https://litterbox.catbox.moe/resources/internals/api.php)
echo "$url" | grep -v "^$" | xclip -selection c
# echo "$url" | sed '/^$/d' | xsel


notify-send "URL copied to clipboard" "Uploaded at: ${url}"
echo "${url} contains file: ${path}" | tee -a "$URL_HIST_FILE"
