#!/usr/bin/env bash
set -euo pipefail # bash strict mode
set -exuo pipefail

sudo apt install -y clojure leiningen

# install babashka
curl -s https://raw.githubusercontent.com/babashka/babashka/master/install | sudo bash --
