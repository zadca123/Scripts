#!/usr/bin/env bash
set -euo pipefail # bash strict mode

# unset -v latest
# for file in "$dir"/*; do
#   [[ $file -nt $latest ]] && latest=$file
# done
dir=$(dirname "$1")
echo $dir
recent=$(find "$dir" -type f -mtime 0 | head -n1)
ext="${1##*.}"
case "$ext" in
    py) black "$1" ;;
    cpp) cd "$dir" && g++ "$1" && ./a.out ;;
    sh) sh "$1" ;;
    lisp) clisp "$1" ;;
    java) javac "$1" && cd "$dir" && java "${recent}" ;;
    *) echo "Unrecognized extension: ${ext}" ;;
esac
