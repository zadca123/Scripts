#!/usr/bin/env bash
set -euo pipefail # bash strict mode

token="$(pass show www/github.com | grep token | cut -d' ' -f2-)"
output="/tmp/latest"
fonts="/tmp/iosevka.zip"

curl \
  -H "Accept: application/vnd.github+json" \
  -H "Authorization: Bearer ${token}" \
  https://api.github.com/repos/be5invis/Iosevka/releases/latest -o "$output"

link="$(awk -F \" '/browser_download_url/ {print $4; exit}' "$output")"

wget "$link" -O "$fonts"
cd /usr/share/fonts
sudo unzip "$fonts"
fc-cache -vf
